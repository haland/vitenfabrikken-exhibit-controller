package no.vitenfabrikken.vec.src.client.model;

import no.vitenfabrikken.vec.src.client.VECClient;
import no.vitenfabrikken.vec.src.client.model.listeners.ConfigurationChangedEvent;
import no.vitenfabrikken.vec.src.client.model.listeners.ConfigurationChangedListener;
import no.vitenfabrikken.vec.src.library.utils.file.ObjectXMLReader;
import no.vitenfabrikken.vec.src.library.utils.file.ObjectXMLWriter;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.File;
import java.util.Iterator;
import java.util.Vector;

@XmlRootElement(name = "client-config")
public class Configuration {
    public static final String CONFIG_FILE_PATH = VECClient.DATA_FOLDER_PATH + File.separator + "config.vec";
    private static Configuration instance = null;
    private int port;
    private String name;
    private String hostIp;
    private boolean alwaysOnline;
    private Vector<ConfigurationChangedListener> listeners;

    public Configuration() {
        super();
        listeners = new Vector<ConfigurationChangedListener>();
    }

    public Configuration(String name, String hostIp, int port, boolean alwaysOnline) {
        this.name = name;
        this.port = port;
        this.hostIp = hostIp;
        this.alwaysOnline = alwaysOnline;

        listeners = new Vector<ConfigurationChangedListener>();
    }

    public static Configuration getCurrentConfiguration() {
        if (instance == null) {
            instance = (Configuration) ObjectXMLReader.read(Configuration.class, new File(CONFIG_FILE_PATH).toPath());
        }

        return instance;
    }

    public static boolean configExists() {
        return new File(CONFIG_FILE_PATH).exists();
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        if (port != this.port) {
            this.port = port;
            notifyConfigurationChangedListeners(new ConfigurationChangedEvent(this,
                    ConfigurationChangedEvent.PORT_CHANGED));
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (!name.equals(this.name)) {
            this.name = name;
            notifyConfigurationChangedListeners(new ConfigurationChangedEvent(this,
                    ConfigurationChangedEvent.NAME_CHANGED));
        }
    }

    public String getHostIp() {
        return hostIp;
    }

    public void setHostIp(String hostIp) {
        if (!hostIp.equals(this.hostIp)) {
            this.hostIp = hostIp;
            notifyConfigurationChangedListeners(new ConfigurationChangedEvent(this,
                    ConfigurationChangedEvent.HOST_IP_CHANGED));
        }
    }

    public boolean isAlwaysOnline() {
        return alwaysOnline;
    }

    public void setAlwaysOnline(boolean alwaysOnline) {
        if (alwaysOnline != this.alwaysOnline) {
            this.alwaysOnline = alwaysOnline;
            notifyConfigurationChangedListeners(new ConfigurationChangedEvent(this,
                    ConfigurationChangedEvent.ALWAYS_ONLINE_CHANGED));
        }
    }

    public void save() {
        File f = new File(CONFIG_FILE_PATH);
        ObjectXMLWriter.write(Configuration.class, this, f);
    }

    private void notifyConfigurationChangedListeners(ConfigurationChangedEvent e) {
        Iterator<ConfigurationChangedListener> i = listeners.iterator();
        while (i.hasNext())
            i.next().configChanged(e);
    }

    public void addConfigurationChangedListener(ConfigurationChangedListener l) {
        if (!listeners.contains(l))
            listeners.add(l);
    }

    public void removeConfigurationChangedListener(ConfigurationChangedListener l) {
        if (listeners.contains(l))
            listeners.remove(l);
    }
}
