package no.vitenfabrikken.vec.src.client.model.listeners;

public interface ConfigurationChangedListener {
    public void configChanged(ConfigurationChangedEvent e);
}
