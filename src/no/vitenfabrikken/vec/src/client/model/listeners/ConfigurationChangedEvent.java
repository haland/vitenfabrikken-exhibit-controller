package no.vitenfabrikken.vec.src.client.model.listeners;

import no.vitenfabrikken.vec.src.client.model.Configuration;

public class ConfigurationChangedEvent {
    public static final int PORT_CHANGED = 0;
    public static final int NAME_CHANGED = 1;
    public static final int HOST_IP_CHANGED = 2;
    public static final int ALWAYS_ONLINE_CHANGED = 3;

    private int propertyChangedId;
    private Configuration config;

    public ConfigurationChangedEvent(Configuration config, int propertyChangedId) {
        this.config = config;
        this.propertyChangedId = propertyChangedId;
    }

    public int getChangedPropertyId() {
        return propertyChangedId;
    }

    public Configuration getSource() {
        return config;
    }
}
