package no.vitenfabrikken.vec.src.client.view.tray;

import no.vitenfabrikken.vec.src.client.VECClient;
import no.vitenfabrikken.vec.src.client.view.dialog.SettingsDialog;
import no.vitenfabrikken.vec.src.client.view.tray.exceptions.SystemTrayNotSupportedException;
import no.vitenfabrikken.vec.src.library.utils.locale.LocaleResource;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ResourceBundle;

public class Tray implements MouseListener, ActionListener {
    private static final int FONT_SIZE = 12;
    private static final String FONT = "Times New Roman";

    private TrayIcon trayIcon;
    private Font regularFont, captiveFont;

    private PopupMenu menu;
    private SystemTray systemTray;
    private ResourceBundle resourceBundle;
    private MenuItem exitMenuItem, settingsMenuItem;

    public Tray() throws SystemTrayNotSupportedException {
        if (SystemTray.isSupported()) {
            try {
                systemTray = SystemTray.getSystemTray();
                resourceBundle = LocaleResource.getLocaleResourceBundle();

                trayIcon = new TrayIcon(VECClient.ICON, resourceBundle.getString("CLIENT_APP_TITLE"));
                trayIcon.setImageAutoSize(true);
                trayIcon.addMouseListener(this);

                captiveFont = new Font(FONT, Font.BOLD, FONT_SIZE);
                regularFont = new Font(FONT, Font.PLAIN, FONT_SIZE);

                menu = new PopupMenu();
                trayIcon.setPopupMenu(menu);

                settingsMenuItem = new MenuItem(resourceBundle.getString("CLIENT_TRAY_SETTINGS"));
                settingsMenuItem.setFont(captiveFont);
                settingsMenuItem.addActionListener(this);
                menu.add(settingsMenuItem);

                menu.addSeparator();

                exitMenuItem = new MenuItem(resourceBundle.getString("CLIENT_TRAY_EXIT"));
                exitMenuItem.setFont(regularFont);
                exitMenuItem.addActionListener(this);
                menu.add(exitMenuItem);

                systemTray.add(trayIcon);
            } catch (AWTException e) {
                throw new SystemTrayNotSupportedException();
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object o = e.getSource();

        if (o.equals(settingsMenuItem)) {
            new SettingsDialog();
        } else if (o.equals(exitMenuItem)) {
            System.exit(0);
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        trayIcon.displayMessage("", resourceBundle.getString("CLIENT_APP_TITLE"), TrayIcon.MessageType.INFO);
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() == 2 && !e.isConsumed()) {
            e.consume();
            new SettingsDialog();
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }
}
