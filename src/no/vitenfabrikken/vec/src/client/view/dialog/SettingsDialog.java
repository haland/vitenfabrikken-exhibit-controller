package no.vitenfabrikken.vec.src.client.view.dialog;

import no.vitenfabrikken.vec.src.client.VECClient;
import no.vitenfabrikken.vec.src.client.controller.log.ClientLogger;
import no.vitenfabrikken.vec.src.client.model.Configuration;
import no.vitenfabrikken.vec.src.library.utils.locale.LocaleResource;
import no.vitenfabrikken.vec.src.library.utils.network.TCPSocket;
import no.vitenfabrikken.vec.src.library.view.dialog.VECDialog;
import no.vitenfabrikken.vec.src.library.view.layout.SpringUtilities;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ResourceBundle;
import java.util.logging.Level;

public class SettingsDialog extends VECDialog implements WindowListener, ActionListener {
    private static final long serialVersionUID = 256539971299513449L;
    private static final int ALLOWED_DIALOG_COUNT = 1;
    private static int DIALOG_COUNT = 0;
    private boolean keepDialog;
    private ResourceBundle resourceBundle;

    private JPanel panel;
    private JButton save, cancel;
    private JCheckBox alwaysOnlineCheckBox;
    private JTextField hostIPText, hostPortText, nameText;
    private JLabel hostIPDescription, hostPortDescription, nameDescription;

    public SettingsDialog() {
        new SettingsDialog(null);
    }

    public SettingsDialog(String message) {
        new SettingsDialog(message, false);
    }

    public SettingsDialog(String message, boolean keepDialog) {
        this.keepDialog = keepDialog;

        Configuration config = null;
        if (Configuration.configExists()) {
            config = Configuration.getCurrentConfiguration();
        }

        if (++DIALOG_COUNT <= ALLOWED_DIALOG_COUNT) {
            resourceBundle = LocaleResource.getLocaleResourceBundle();

            panel = new JPanel(new SpringLayout());

            nameDescription = new JLabel(resourceBundle.getString("CLIENT_SETTINGS_DESCRIPTION_NAME"));
            panel.add(nameDescription);

            nameText = new JTextField((config != null) ? config.getName() : "");
            panel.add(nameText);

            hostIPDescription = new JLabel(resourceBundle.getString("CLIENT_SETTINGS_HOST_IP"));
            panel.add(hostIPDescription);

            hostIPText = new JTextField(message != null ? message : (config != null) ? config.getHostIp() : "");
            panel.add(hostIPText);

            hostPortDescription = new JLabel(resourceBundle.getString("CLIENT_SETTINGS_PORT"));
            panel.add(hostPortDescription);

            hostPortText = new JTextField((config != null) ? String.valueOf(config.getPort()) : "");
            panel.add(hostPortText);

            alwaysOnlineCheckBox = new JCheckBox(resourceBundle.getString("CLIENT_SETTINGS_ALWAYS_ONLINE"),
                    (config != null) ? config.isAlwaysOnline() : false);
            panel.add(alwaysOnlineCheckBox);

            save = new JButton(resourceBundle.getString("CLIENT_SETTINGS_SAVE"));
            save.addActionListener(this);
            panel.add(save);

            cancel = new JButton(keepDialog ? resourceBundle.getString("CLIENT_TRAY_EXIT") :
                    resourceBundle.getString("CLIENT_SETTINGS_CANCEL"));
            cancel.addActionListener(this);
            panel.add(cancel);

            SpringUtilities.makeCompactGrid(panel, panel.getComponentCount(), 1, 5, 5, 5, 5);
            add(panel);

            pack();
            setPosition();
            setIconImage(VECClient.ICON);

            setTitle(resourceBundle.getString("CLIENT_SETTINGS_TITLE"));
            setResizable(false);
            addWindowListener(this);
            setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

            setVisible(true);
        } else {
            dispose();
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(save)) {
            if (Configuration.configExists()) {
                Configuration config = Configuration.getCurrentConfiguration();

                config.setName(nameText.getText());
                config.setHostIp(hostIPText.getText());
                config.setAlwaysOnline(alwaysOnlineCheckBox.isSelected());

                try {
                    int port = Integer.parseInt(hostPortText.getText());
                    if (TCPSocket.available(port))
                        config.setPort(port);
                } catch (NumberFormatException arg) {
                    ClientLogger.logMessage(Level.FINE, "Text entered as port is not a number");
                }

                VECClient.getInstance();
                windowClosing(new WindowEvent(this, 1));
            } else {
                try {
                    int port = Integer.parseInt(hostPortText.getText());

                    if (TCPSocket.available(port)) {
                        Configuration config = new Configuration(nameText.getText(), hostIPText.getText(), port,
                                alwaysOnlineCheckBox.isSelected());
                        config.save();

                        VECClient.getInstance();
                        windowClosing(new WindowEvent(this, 1));
                    } else {
                        hostPortText.setText(resourceBundle.getString("CLIENT_SETTINGS_ILLEGAL_PORT"));
                    }
                } catch (NumberFormatException arg) {
                    hostPortText.setText(resourceBundle.getString("CLIENT_SETTINGS_ILLEGAL_PORT"));
                }
            }
        } else if (e.getSource().equals(cancel)) {
            windowClosing(new WindowEvent(this, 1));
        }
    }

    public void windowClosing(WindowEvent e) {
        if (!keepDialog) {
            DIALOG_COUNT--;
            dispose();
        }
    }

    public void windowOpened(WindowEvent e) {
    }

    public void windowClosed(WindowEvent e) {
    }

    public void windowActivated(WindowEvent e) {
    }

    public void windowIconified(WindowEvent e) {
    }

    public void windowDeactivated(WindowEvent e) {
    }

    public void windowDeiconified(WindowEvent e) {
    }
}
