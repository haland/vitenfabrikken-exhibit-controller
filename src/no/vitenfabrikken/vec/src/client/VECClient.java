package no.vitenfabrikken.vec.src.client;

import no.vitenfabrikken.vec.src.client.controller.command.CommandHandler;
import no.vitenfabrikken.vec.src.client.controller.config.ConfigHandler;
import no.vitenfabrikken.vec.src.client.controller.log.ClientLogger;
import no.vitenfabrikken.vec.src.client.controller.network.MessageHandler;
import no.vitenfabrikken.vec.src.client.controller.network.NetworkHandler;
import no.vitenfabrikken.vec.src.client.controller.status.StatusOnStartupSender;
import no.vitenfabrikken.vec.src.client.model.Configuration;
import no.vitenfabrikken.vec.src.client.view.dialog.SettingsDialog;
import no.vitenfabrikken.vec.src.client.view.tray.Tray;
import no.vitenfabrikken.vec.src.client.view.tray.exceptions.SystemTrayNotSupportedException;
import no.vitenfabrikken.vec.src.library.msg.Command;
import no.vitenfabrikken.vec.src.library.utils.network.card.NetworkCard;
import no.vitenfabrikken.vec.src.library.utils.network.card.exceptions.NetworkCardNotFoundException;
import no.vitenfabrikken.vec.src.library.utils.network.channel.Channel;
import no.vitenfabrikken.vec.src.library.utils.network.serializable.Message;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.logging.Level;

public class VECClient {
    public static final Image ICON = new ImageIcon("images/tray-icon.png").getImage();
    public static String DATA_FOLDER_PATH = System.getProperty("os.name").toLowerCase().contains("windows") ?
            System.getenv("APPDATA") + File.separator + ".vec-client" :
            System.getProperty("user.home") + File.separator + ".vec-client";
    private static VECClient instance;

    public VECClient() {
        Configuration config = Configuration.getCurrentConfiguration();
        new ConfigHandler(config);

        try {
            new Tray();
        } catch (SystemTrayNotSupportedException e) {
            new SettingsDialog(null, true);
        }

        NetworkCard networkCard = null;
        do {
            try {
                networkCard = new NetworkCard(NetworkCard.getSubNetFromIP(config.getHostIp()));
            } catch (NetworkCardNotFoundException e) {
                ClientLogger.logMessage(Level.FINEST, "A network interface for subset '" +
                        NetworkCard.getSubNetFromIP(config.getHostIp()) + "255' could not be found, waiting for " +
                        "interface: " + e.getLocalizedMessage());
            }
        } while (networkCard == null);

        Channel<Message> messageChannel = new Channel<Message>(10);
        Channel<Command> commandChannel = new Channel<Command>(10);
        NetworkHandler networkHandler = new NetworkHandler(config, messageChannel);

        new StatusOnStartupSender(config, networkCard, networkHandler);

        new MessageHandler(messageChannel, commandChannel);
        new CommandHandler(config, commandChannel, networkHandler, networkCard);
    }

    public static VECClient getInstance() {
        if (instance == null) {
            instance = new VECClient();
        }
        return instance;
    }

    public static void main(String[] args) {
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                String stackTrace = "";
                StackTraceElement[] stackTraceElements = e.getStackTrace();

                for (StackTraceElement stackTraceElement : stackTraceElements) {
                    stackTrace = stackTrace + stackTraceElement.getClassName() + "\n" +
                            "   at " + stackTraceElement.getClassName() + "." +
                            stackTraceElement.getMethodName() + "(" + stackTraceElement.getFileName() +
                            ":" + stackTraceElement.getLineNumber() + ")\n";
                }

                ClientLogger.logMessage(Level.SEVERE, "Uncaught exception in thread '" + t.getName() + "': " +
                        stackTrace);
            }
        });

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            ClientLogger.logMessage(Level.FINE, "Could not set UI to match that of the OS: " + e.getLocalizedMessage());
        }

        File dataFolder = new File(DATA_FOLDER_PATH);
        if (!dataFolder.exists())
            dataFolder.mkdirs();

        if (!Configuration.configExists()) {
            new SettingsDialog();
        } else {
            getInstance();
        }
    }
}