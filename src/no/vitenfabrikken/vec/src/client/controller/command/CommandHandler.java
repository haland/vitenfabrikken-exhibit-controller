package no.vitenfabrikken.vec.src.client.controller.command;

import no.vitenfabrikken.vec.src.client.controller.log.ClientLogger;
import no.vitenfabrikken.vec.src.client.controller.network.NetworkHandler;
import no.vitenfabrikken.vec.src.client.model.Configuration;
import no.vitenfabrikken.vec.src.library.msg.Command;
import no.vitenfabrikken.vec.src.library.msg.Status;
import no.vitenfabrikken.vec.src.library.utils.network.card.NetworkCard;
import no.vitenfabrikken.vec.src.library.utils.network.channel.Channel;

import java.io.IOException;
import java.net.ConnectException;
import java.util.logging.Level;

public class CommandHandler implements Runnable {
    private static final String THREAD_NAME = "CommandHandler";

    private int status;
    private Thread thread;
    private Configuration config;
    private NetworkCard networkCard;
    private NetworkHandler networkHandler;
    private Channel<Command> commandChannel;

    public CommandHandler(Configuration config, Channel<Command> commandChannel, NetworkHandler networkHandler,
                          NetworkCard networkCard) {
        this.config = config;
        this.networkCard = networkCard;
        this.networkHandler = networkHandler;
        this.commandChannel = commandChannel;

        status = Status.ONLINE;

        thread = new Thread(this);
        thread.setName(THREAD_NAME);
        thread.start();
    }

    @Override
    public void run() {
        while (true) {
            try {
                Command c = commandChannel.take();

                if (c != null) {
                    if (c.getCommand().equals(Command.RESTART))
                        restart();
                    else if (c.getCommand().equals(Command.SHUTDOWN))
                        shutdown();
                    else if (c.getCommand().equals(Command.CONFIGURATION))
                        sendStatus();
                }
            } catch (InterruptedException e) {
                ClientLogger.logMessage(Level.FINE, "CommandHandler was interrupted. Retrying listening for commands.");
            }
        }
    }

    private void restart() {
        try {
            status = Status.RESTARTING;
            networkHandler.getSocket().send(config.getHostIp(), new Status(config.getName(),
                    networkCard.getIPAddress(), networkCard.getMACAddress(), status, Status.NO_ERROR));
        } catch (ConnectException e) {
            ClientLogger.logMessage(Level.FINE, "Could not send status on current connection.");
        }

        String os = System.getProperty("os.name").toLowerCase();
        Runtime runtime = Runtime.getRuntime();
        try {
            if (os.contains("windows"))
                runtime.exec("shutdown -r -f -t 0");
            else
                runtime.exec("shutdown -r now");
        } catch (IOException e) {
            ClientLogger.logMessage(Level.WARNING, "Was not able to complete restart command.");
        }
    }

    private void shutdown() {
        if (!config.isAlwaysOnline()) {
            try {
                status = Status.SHUTTING_DOWN;
                networkHandler.getSocket().send(config.getHostIp(), new Status(config.getName(),
                        networkCard.getIPAddress(), networkCard.getMACAddress(), status, Status.NO_ERROR));
            } catch (ConnectException e) {
                ClientLogger.logMessage(Level.FINE, "Could not send status on current connection.");
            }

            String os = System.getProperty("os.name").toLowerCase();
            Runtime runtime = Runtime.getRuntime();
            try {
                if (os.contains("windows"))
                    runtime.exec("shutdown -s -f -t 0");
                else
                    runtime.exec("shutdown -h now");
            } catch (IOException e) {
                ClientLogger.logMessage(Level.WARNING, "Was not able to complete shutdown command.");
            }
        } else {
            try {
                networkHandler.getSocket().send(config.getHostIp(), new Command(Command.SHUTDOWN));
            } catch (ConnectException e) {
                ClientLogger.logMessage(Level.FINE, "Could not send shutdown command on current connection.");
            }
        }
    }

    private void sendStatus() {
        try {
            int status = (config.isAlwaysOnline()) ? Status.ALWAYS_ON : this.status;
            networkHandler.getSocket().send(config.getHostIp(), new Status(config.getName(), networkCard.getIPAddress(),
                    networkCard.getMACAddress(), status, Status.NO_ERROR));
        } catch (ConnectException e) {
            ClientLogger.logMessage(Level.FINE, "Could not send status on current connection.");
        }
    }
}
