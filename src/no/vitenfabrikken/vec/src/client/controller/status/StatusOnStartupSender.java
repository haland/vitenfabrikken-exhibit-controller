package no.vitenfabrikken.vec.src.client.controller.status;

import no.vitenfabrikken.vec.src.client.controller.log.ClientLogger;
import no.vitenfabrikken.vec.src.client.controller.network.NetworkHandler;
import no.vitenfabrikken.vec.src.client.model.Configuration;
import no.vitenfabrikken.vec.src.library.msg.Status;
import no.vitenfabrikken.vec.src.library.utils.network.card.NetworkCard;

import java.net.ConnectException;
import java.util.logging.Level;

public class StatusOnStartupSender implements Runnable {
    private static final String THREAD_NAME = "StatusOnStartupSender";
    private Thread thread;
    private Configuration config;
    private NetworkCard networkCard;
    private NetworkHandler networkHandler;

    public StatusOnStartupSender(Configuration config, NetworkCard networkCard, NetworkHandler networkHandler) {
        this.config = config;
        this.networkCard = networkCard;
        this.networkHandler = networkHandler;

        thread = new Thread(this);
        thread.setName(THREAD_NAME);
        thread.start();
    }

    @Override
    public void run() {
        boolean msgSent = false;

        while (!msgSent) {
            try {
                int status = (config.isAlwaysOnline()) ? Status.ALWAYS_ON : Status.ONLINE;
                networkHandler.getSocket().send(config.getHostIp(), new Status(config.getName(),
                        networkCard.getIPAddress(), networkCard.getMACAddress(), status, Status.NO_ERROR));
                msgSent = true;
            } catch (ConnectException e) {
                ClientLogger.logMessage(Level.FINE, "Was not able to send status over the current connection.");
            }
        }
    }
}
