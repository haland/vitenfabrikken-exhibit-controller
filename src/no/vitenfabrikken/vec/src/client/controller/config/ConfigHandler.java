package no.vitenfabrikken.vec.src.client.controller.config;

import no.vitenfabrikken.vec.src.client.model.Configuration;
import no.vitenfabrikken.vec.src.client.model.listeners.ConfigurationChangedEvent;
import no.vitenfabrikken.vec.src.client.model.listeners.ConfigurationChangedListener;

public class ConfigHandler implements ConfigurationChangedListener {

    public ConfigHandler(Configuration configuration) {
        configuration.addConfigurationChangedListener(this);
    }

    @Override
    public void configChanged(ConfigurationChangedEvent e) {
        e.getSource().save();
    }
}
