package no.vitenfabrikken.vec.src.client.controller.network;

import no.vitenfabrikken.vec.src.client.controller.log.ClientLogger;
import no.vitenfabrikken.vec.src.client.model.Configuration;
import no.vitenfabrikken.vec.src.client.model.listeners.ConfigurationChangedEvent;
import no.vitenfabrikken.vec.src.client.model.listeners.ConfigurationChangedListener;
import no.vitenfabrikken.vec.src.library.utils.network.TCPSocket;
import no.vitenfabrikken.vec.src.library.utils.network.channel.Channel;
import no.vitenfabrikken.vec.src.library.utils.network.serializable.Message;

import java.io.IOException;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;

public class NetworkHandler implements ConfigurationChangedListener {
    private TCPSocket socket;
    private Semaphore semaphore;
    private boolean configChanged;
    private Channel<Message> channel;

    public NetworkHandler(Configuration configuration, Channel<Message> channel) {
        this.channel = channel;

        configChanged = false;
        semaphore = new Semaphore(0);
        try {
            socket = new TCPSocket(configuration.getPort(), channel);
        } catch (IOException e) {
            ClientLogger.logMessage(Level.WARNING, "Network connection could not be established: " +
                    e.getLocalizedMessage());
        }

        configuration.addConfigurationChangedListener(this);
    }

    public TCPSocket getSocket() {
        if (configChanged) {
            try {
                semaphore.acquire();
            } catch (InterruptedException e) {
                ClientLogger.logMessage(Level.FINE, "Failed to acquire semaphore in NetworkHandler.");
            }
        }

        return socket;
    }

    @Override
    public void configChanged(ConfigurationChangedEvent e) {
        if (e.getChangedPropertyId() == ConfigurationChangedEvent.PORT_CHANGED) {
            configChanged = true;
            socket.close();

            try {
                socket = new TCPSocket(e.getSource().getPort(), channel);
            } catch (IOException e1) {
                ClientLogger.logMessage(Level.WARNING, "Network connection could not be established: " +
                        e1.getLocalizedMessage());
            }
            configChanged = false;

            for (int i = 0; i < semaphore.getQueueLength(); i++)
                semaphore.release();
        }
    }
}
