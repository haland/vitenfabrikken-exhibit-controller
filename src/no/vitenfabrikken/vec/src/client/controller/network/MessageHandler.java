package no.vitenfabrikken.vec.src.client.controller.network;

import no.vitenfabrikken.vec.src.client.controller.log.ClientLogger;
import no.vitenfabrikken.vec.src.library.msg.Command;
import no.vitenfabrikken.vec.src.library.utils.network.channel.Channel;
import no.vitenfabrikken.vec.src.library.utils.network.serializable.Message;

import java.util.logging.Level;

public class MessageHandler implements Runnable {
    private static final String THREAD_NAME = "MessageHandler";

    private Thread thread;
    private Channel<Command> commandChannel;
    private Channel<Message> messageChannel;

    public MessageHandler(Channel<Message> messageChannel, Channel<Command> commandChannel) {
        this.commandChannel = commandChannel;
        this.messageChannel = messageChannel;

        thread = new Thread(this);
        thread.setName(THREAD_NAME);
        thread.start();
    }

    @Override
    public void run() {
        while (true) {
            try {
                Message m = messageChannel.take();

                if (m.getObject() instanceof Command) {
                    commandChannel.put((Command) m.getObject());
                }
            } catch (InterruptedException e) {
                ClientLogger.logMessage(Level.FINE, "MessageHandler was interrupted. Retrying listening for messages.");
            }
        }
    }
}
