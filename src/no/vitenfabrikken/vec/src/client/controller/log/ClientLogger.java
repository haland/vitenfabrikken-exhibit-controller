package no.vitenfabrikken.vec.src.client.controller.log;

import no.vitenfabrikken.vec.src.client.VECClient;
import no.vitenfabrikken.vec.src.library.utils.logger.Log;

import java.util.logging.Level;

public class ClientLogger extends Log {
    private static final Level CONSOLE_LOG_LEVEL = Level.FINEST;
    private static final Level FILE_LOG_LEVEL = Level.SEVERE;
    private static final String LOG_FILE_DESTINATION = VECClient.DATA_FOLDER_PATH;
    private static ClientLogger instance;

    private ClientLogger() {
        super(CONSOLE_LOG_LEVEL, FILE_LOG_LEVEL, LOG_FILE_DESTINATION, ClientLogger.class.getName());
    }

    public static void logMessage(Level level, String message) {
        getInstance().writeMessage(level, message);
    }

    public static void logStackTrace(Level level, StackTraceElement[] stackTraceElements) {
        getInstance().writeStackTrace(level, stackTraceElements);
    }

    private static ClientLogger getInstance() {
        if (instance == null) {
            instance = new ClientLogger();
        }

        return instance;
    }
}
