package no.vitenfabrikken.vec.src.server.controller.machine.actions;

import no.vitenfabrikken.vec.src.library.utils.locale.LocaleResource;
import no.vitenfabrikken.vec.src.server.model.config.Configuration;
import no.vitenfabrikken.vec.src.server.model.machine.Computer;
import no.vitenfabrikken.vec.src.server.model.machine.Machine;
import no.vitenfabrikken.vec.src.server.view.dialog.progress.ProgressBarDialog;
import no.vitenfabrikken.vec.src.server.view.window.button.listeners.MachineButtonListener;

import javax.swing.*;
import java.util.List;
import java.util.ResourceBundle;


public class MachineShutdownListener implements MachineButtonListener {
    private Machine[] machines;
    private Configuration config;
    private ProgressBarDialog dialog;
    private ResourceBundle resourceBundle;

    @Override
    public void buttonPressed(Machine[] machines) {
        this.machines = machines;
        this.resourceBundle = LocaleResource.getLocaleResourceBundle();

        dialog = new ProgressBarDialog(resourceBundle.getString("MACHINE_SHUTDOWN_LISTENER_TITLE"), machines.length);
        new TurnAllOff().execute();
    }

    private class TurnAllOff extends SwingWorker<String, Integer> {

        protected String doInBackground() throws Exception {
            config = Configuration.getCurrentConfiguration();
            for (int i = 0; i < machines.length; i++) {
                Machine m = machines[i];
                dialog.setProgress(m.getName());

                if (m instanceof Computer) {
                    Computer c = (Computer) m;
                    c.setLastUpdatedStatusAt(System.currentTimeMillis());

                    if (!m.getStatus().equals(resourceBundle.getString("STATUS_OFFLINE")) &&
                            !c.getStatus().equals(config.getAlwaysOnlineText())) {
                        c.setStatus(resourceBundle.getString("STATUS_SHUTTING_DOWN"));
                    }
                }

                m.shutdown();

                publish(i + 1);
            }

            return null;
        }

        protected void process(List<Integer> chunks) {
            for (Integer percent : chunks)
                dialog.setPercent(percent);
            super.process(chunks);
        }
    }
}
