package no.vitenfabrikken.vec.src.server.controller.machine;

import no.vitenfabrikken.vec.src.library.msg.Status;
import no.vitenfabrikken.vec.src.library.utils.file.ObjectXMLReader;
import no.vitenfabrikken.vec.src.library.utils.file.ObjectXMLWriter;
import no.vitenfabrikken.vec.src.library.utils.locale.LocaleResource;
import no.vitenfabrikken.vec.src.library.utils.network.channel.Channel;
import no.vitenfabrikken.vec.src.server.VECServer;
import no.vitenfabrikken.vec.src.server.controller.log.ServerLogger;
import no.vitenfabrikken.vec.src.server.model.config.Configuration;
import no.vitenfabrikken.vec.src.server.model.list.MachineList;
import no.vitenfabrikken.vec.src.server.model.list.listeners.MachineListChangedEvent;
import no.vitenfabrikken.vec.src.server.model.list.listeners.MachineListChangedListener;
import no.vitenfabrikken.vec.src.server.model.machine.Computer;
import no.vitenfabrikken.vec.src.server.model.machine.Machine;
import no.vitenfabrikken.vec.src.server.model.machine.listeners.MachineSettingsChangedEvent;
import no.vitenfabrikken.vec.src.server.model.machine.listeners.MachineSettingsChangedListener;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.Normalizer;
import java.util.ResourceBundle;
import java.util.logging.Level;

public class MachineHandler implements Runnable, MachineSettingsChangedListener, MachineListChangedListener {
    private static final String THREAD_NAME = "MachineHandler";

    private Thread thread;
    private MachineList machineList;
    private ResourceBundle resourceBundle;
    private Channel<Status> statusChannel;

    public MachineHandler(Channel<Status> statusChannel) {
        this.statusChannel = statusChannel;

        resourceBundle = LocaleResource.getLocaleResourceBundle();

        machineList = new MachineList();
        machineList.addMachineListChangedListener(this);


        File folder = new File(VECServer.MACHINES_FOLDER_PATH);
        Path machineFolderPath = folder.toPath();

        DirectoryStream<Path> directoryStream = null;

        try {
            directoryStream = Files.newDirectoryStream(machineFolderPath);

            for (Path filePath : directoryStream) {
                if (filePath.getFileName().toString().endsWith(VECServer.FILE_TYPE)) {
                    if (!emptyFile(filePath)) {
                        Machine m = (Machine) ObjectXMLReader.read(Machine.class, filePath);

                        if (m != null) {
                            machineList.add(m);
                        }
                    } else {
                        Files.delete(filePath);

                        ServerLogger.logMessage(Level.WARNING, "\"" + filePath.getFileName() + "\" will be deleted on " +
                                "exit because it was empty. If the file represents a computer, manually start the " +
                                "machine and it  should reintroduce itself to the system. If the file represents a " +
                                "projector, add the projector manually by using \"Add new projector\".");
                        System.out.println("\"" + filePath.getFileName() + "\" will be deleted on " +
                                "exit because it was empty. If the file represents a computer, manually start the " +
                                "machine and it  should reintroduce itself to the system. If the file represents a " +
                                "projector, add the projector manually by using \"Add new projector\".");
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (directoryStream != null) {
                try {
                    directoryStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        thread = new Thread(this);
        thread.setName(THREAD_NAME);
        thread.start();
    }

    public static String getFilePath(String objectName) {
        String normalizedName = Normalizer.normalize(objectName, Normalizer.Form.NFD);
        String plainName = normalizedName.replaceAll("[^\\sa-zA-Z0-9]", "");
        String lowerCaseName = plainName.toLowerCase();
        String fileName = lowerCaseName.replace(" ", "_");

        return VECServer.MACHINES_FOLDER_PATH + File.separator + fileName + ".vec";
    }

    @Override
    public void settingsChange(MachineSettingsChangedEvent e) {
        if (e.getType() == MachineSettingsChangedEvent.NAME) {
            File oldFile = new File(getFilePath((String) e.getOldValue()));
            oldFile.delete();
        }

        Machine m = e.getSource();
        File file = new File(getFilePath(m.getName()));
        ObjectXMLWriter.write(Machine.class, m, file);
    }

    @Override
    public void machineListChanged(MachineListChangedEvent e) {
        if (e.getType() == MachineListChangedEvent.REMOVED) {
            e.getSource().removeSettingsChangedListener(this);
        } else if (e.getType() == MachineListChangedEvent.ADDED) {
            e.getSource().addSettingsChangedListener(this);

            File file = new File(getFilePath(e.getSource().getName()));
            if (!file.exists())
                ObjectXMLWriter.write(Machine.class, e.getSource(), file);
        }
    }

    @Override
    public void run() {
        while (true) {
            try {
                Status s = statusChannel.take();
                Computer c = (Computer) machineList.contains(s.getMac());

                if (c != null) {
                    c.setIp(s.getIp());
                    c.setMac(s.getMac());
                    c.setName(s.getName());
                    c.setLastUpdatedStatusAt(System.currentTimeMillis());

                    switch (s.getError()) {
                        case Status.NO_ERROR:
                            c.setError(resourceBundle.getString("ERROR_NO_ERROR"));
                            break;
                        default:
                            c.setError(resourceBundle.getString("ERROR_NOT_AVAILABLE"));
                            break;
                    }

                    switch (s.getStatus()) {
                        case Status.ONLINE:
                            c.setStatus(resourceBundle.getString("STATUS_ONLINE"));
                            break;
                        case Status.RESTARTING:
                            c.setStatus(resourceBundle.getString("STATUS_RESTARTING"));
                            break;
                        case Status.SHUTTING_DOWN:
                            c.setStatus(resourceBundle.getString("STATUS_SHUTTING_DOWN"));
                            break;
                        case Status.ALWAYS_ON:
                            Configuration config = Configuration.getCurrentConfiguration();
                            String statusChange = config.getAlwaysOnlineText();
                            c.setStatus(statusChange);
                            break;
                        default:
                            c.setStatus(resourceBundle.getString("STATUS_NOT_AVAILABLE"));
                            break;
                    }
                } else {
                    c = new Computer(s.getIp(), s.getMac(), s.getName());
                    machineList.add(c);
                }
            } catch (InterruptedException e) {
                ServerLogger.logMessage(Level.FINEST, "Thread interrupted while waiting for status: " +
                        e.getLocalizedMessage());
                e.printStackTrace();
            }
        }
    }

    private boolean emptyFile(Path path) {
        BufferedReader bufferedReader = null;

        try {
            bufferedReader = Files.newBufferedReader(path, Charset.forName("UTF-8"));

            return (bufferedReader.readLine() == null);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return true;
    }

    public MachineList getMachineList() {
        return machineList;
    }
}
