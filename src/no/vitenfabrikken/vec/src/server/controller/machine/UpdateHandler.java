package no.vitenfabrikken.vec.src.server.controller.machine;

import no.vitenfabrikken.vec.src.library.msg.Command;
import no.vitenfabrikken.vec.src.library.utils.locale.LocaleResource;
import no.vitenfabrikken.vec.src.server.controller.log.ServerLogger;
import no.vitenfabrikken.vec.src.server.controller.network.NetworkHandler;
import no.vitenfabrikken.vec.src.server.model.list.MachineList;
import no.vitenfabrikken.vec.src.server.model.machine.Computer;
import no.vitenfabrikken.vec.src.server.model.machine.Machine;
import no.vitenfabrikken.vec.src.server.model.machine.Projector;

import java.net.ConnectException;
import java.util.ResourceBundle;
import java.util.logging.Level;

public class UpdateHandler implements Runnable {
    private static final String THREAD_NAME = "UpdateHandler";
    private static final int DELAY = 80;
    private static final long TIMEOUT = 3 * 60 * 1000; //3 minutes timeout before a computer is determined as faulty if
    //no connection has been established within that time.

    private boolean shutdownSignal = false;

    private Thread thread;
    private MachineList machineList;
    private NetworkHandler networkHandler;
    private ResourceBundle resourceBundle;

    public UpdateHandler(NetworkHandler networkHandler, MachineList machineList) {
        this.machineList = machineList;
        this.networkHandler = networkHandler;

        resourceBundle = LocaleResource.getLocaleResourceBundle();

        thread = new Thread(this);
        thread.setName(THREAD_NAME);
        thread.start();
    }

    public void shutdown() {
        shutdownSignal = true;
    }

    @Override
    public void run() {
        while (!shutdownSignal) {
            try {
                Machine[] machines = machineList.toArray(new Machine[machineList.size()]);

                for (Machine m : machines) {
                    if (m instanceof Computer) {
                        Computer c = (Computer) m;

                        try {
                            networkHandler.getSocket().send(c.getIp(), new Command(Command.CONFIGURATION));
                        } catch (ConnectException e) {
                            if ((c.getStatus().equals(resourceBundle.getString("STATUS_STARTING_UP")) ||
                                    c.getStatus().equals(resourceBundle.getString("STATUS_RESTARTING"))) &&
                                    (System.currentTimeMillis() - c.getLastUpdatedStatusAt()) > TIMEOUT) {
                                c.setError(resourceBundle.getString("ERROR_UNABLE_TO_START"));
                            }

                            if (!c.getStatus().equals(resourceBundle.getString("STATUS_STARTING_UP")) &&
                                    !c.getStatus().equals(resourceBundle.getString("STATUS_RESTARTING"))) {
                                c.setStatus(resourceBundle.getString("STATUS_OFFLINE"));
                            }
                        }
                    } else if (m instanceof Projector) {
                        Projector p = (Projector) m;
                        p.refresh();
                    }

                    try {
                        Thread.sleep(DELAY);
                    } catch (InterruptedException e) {
                        ServerLogger.logMessage(Level.FINEST, "Thread could not sleep at: " + e.getMessage());
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                ServerLogger.logStackTrace(Level.WARNING, e.getStackTrace());
                e.printStackTrace();
            }
        }
    }
}
