package no.vitenfabrikken.vec.src.server.controller.machine.actions;

import no.vitenfabrikken.vec.src.library.utils.locale.LocaleResource;
import no.vitenfabrikken.vec.src.server.model.machine.Computer;
import no.vitenfabrikken.vec.src.server.model.machine.Machine;
import no.vitenfabrikken.vec.src.server.view.dialog.progress.ProgressBarDialog;
import no.vitenfabrikken.vec.src.server.view.window.button.listeners.MachineButtonListener;

import javax.swing.*;
import java.util.List;
import java.util.ResourceBundle;


public class MachineWakeListener implements MachineButtonListener {
    private Machine[] machines;
    private ProgressBarDialog dialog;
    private ResourceBundle resourceBundle;

    public MachineWakeListener() {
        resourceBundle = LocaleResource.getLocaleResourceBundle();
    }

    @Override
    public void buttonPressed(Machine[] machines) {
        this.machines = machines;

        dialog = new ProgressBarDialog(resourceBundle.getString("MACHINE_WAKE_LISTENER_TITLE"), machines.length);
        new TurnAllOff().execute();
    }

    private class TurnAllOff extends SwingWorker<String, Integer> {

        protected String doInBackground() throws Exception {

            for (int i = 0; i < machines.length; i++) {
                Machine m = machines[i];
                dialog.setProgress(m.getName());

                if (m instanceof Computer) {
                    Computer c = (Computer) m;
                    c.setLastUpdatedStatusAt(System.currentTimeMillis());
                    c.setStatus(resourceBundle.getString("STATUS_STARTING_UP"));
                    c.setError(resourceBundle.getString("ERROR_NO_ERROR"));
                }

                m.wake();

                publish(i + 1);
            }

            return null;
        }

        protected void process(List<Integer> chunks) {
            for (Integer percent : chunks)
                dialog.setPercent(percent);
            super.process(chunks);
        }
    }
}
