package no.vitenfabrikken.vec.src.server.controller.config;

import no.vitenfabrikken.vec.src.server.model.config.Configuration;
import no.vitenfabrikken.vec.src.server.model.config.listeners.ConfigurationChangedEvent;
import no.vitenfabrikken.vec.src.server.model.config.listeners.ConfigurationChangedListener;

public class ConfigHandler implements ConfigurationChangedListener {

    public ConfigHandler(Configuration configuration) {
        configuration.addConfigurationChangedListener(this);
    }

    @Override
    public void configChanged(ConfigurationChangedEvent e) {
        e.getSource().save();
    }
}
