package no.vitenfabrikken.vec.src.server.controller.network;

import no.vitenfabrikken.vec.src.library.msg.Command;
import no.vitenfabrikken.vec.src.library.msg.Status;
import no.vitenfabrikken.vec.src.library.utils.network.channel.Channel;
import no.vitenfabrikken.vec.src.library.utils.network.serializable.Message;
import no.vitenfabrikken.vec.src.server.controller.log.ServerLogger;

import java.util.logging.Level;

public class MessageHandler implements Runnable {
    private static final String THREAD_NAME = "MessageHandler";

    private Thread thread;
    private Channel<Status> statusChannel;
    private Channel<Message> messageChannel;

    public MessageHandler(Channel<Message> messageChannel, Channel<Status> statusChannel) {
        this.statusChannel = statusChannel;
        this.messageChannel = messageChannel;

        thread = new Thread(this);
        thread.setName(THREAD_NAME);
        thread.start();
    }

    @Override
    public void run() {
        while (true) {
            try {
                Message m = messageChannel.take();

                if (m != null && m.getObject() != null) {
                    if (m.getObject() instanceof Status) {
                        statusChannel.put((Status) m.getObject());
                    } else if (m.getObject() instanceof Command) {
                        System.err.println(m.getIP() + " responded with command: " +
                                ((Command) m.getObject()).getCommand());
                    }
                }
            } catch (InterruptedException e) {
                ServerLogger.logMessage(Level.FINEST, "Thread interrupted while waiting on message: " +
                        e.getLocalizedMessage());
                e.printStackTrace();
            }
        }
    }
}
