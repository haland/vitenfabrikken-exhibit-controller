package no.vitenfabrikken.vec.src.server.controller.network;

import no.vitenfabrikken.vec.src.library.utils.inst.InstCtrl;
import no.vitenfabrikken.vec.src.library.utils.network.TCPSocket;
import no.vitenfabrikken.vec.src.library.utils.network.channel.Channel;
import no.vitenfabrikken.vec.src.library.utils.network.exceptions.PortInUseException;
import no.vitenfabrikken.vec.src.library.utils.network.serializable.Message;
import no.vitenfabrikken.vec.src.server.VECServer;
import no.vitenfabrikken.vec.src.server.controller.log.ServerLogger;
import no.vitenfabrikken.vec.src.server.model.config.Configuration;
import no.vitenfabrikken.vec.src.server.model.config.listeners.ConfigurationChangedEvent;
import no.vitenfabrikken.vec.src.server.model.config.listeners.ConfigurationChangedListener;

import java.io.IOException;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;

public class NetworkHandler implements ConfigurationChangedListener {
    private TCPSocket socket;
    private Semaphore semaphore;
    private boolean configChanged;
    private Channel<Message> channel;

    public NetworkHandler(Configuration configuration, InstCtrl invoker, Channel<Message> channel) {
        this.channel = channel;

        configChanged = false;
        semaphore = new Semaphore(0);

        try {
            socket = new TCPSocket(configuration.getPort(), channel);
        } catch (PortInUseException e) {
            invoker.terminate();
            InstCtrl.invokeInstance(VECServer.class, true);
            System.exit(0);
        } catch (IOException e) {
            ServerLogger.logMessage(Level.WARNING, "Network connection could not be established: " +
                    e.getLocalizedMessage());
            e.printStackTrace();
        }

        configuration.addConfigurationChangedListener(this);
    }

    public void shutdown() {
        socket.close();
    }

    public TCPSocket getSocket() {
        if (configChanged) {
            try {
                semaphore.acquire();
            } catch (InterruptedException e) {
                ServerLogger.logMessage(Level.SEVERE, "Threads could not block-wait until new TCPSocket was created: " +
                        e.getLocalizedMessage());
                e.printStackTrace();
            }
        }

        return socket;
    }

    @Override
    public void configChanged(ConfigurationChangedEvent e) {
        if (e.getChangedPropertyId() == ConfigurationChangedEvent.PORT_CHANGED) {
            configChanged = true;
            socket.close();

            try {
                socket = new TCPSocket(e.getSource().getPort(), channel);
            } catch (IOException e1) {
                ServerLogger.logMessage(Level.WARNING, "Network connection could not be established: " +
                        e1.getLocalizedMessage());
                e1.printStackTrace();
            }
            configChanged = false;

            for (int i = 0; i < semaphore.getQueueLength(); i++)
                semaphore.release();
        }
    }
}
