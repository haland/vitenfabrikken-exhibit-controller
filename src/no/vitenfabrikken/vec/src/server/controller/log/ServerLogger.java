package no.vitenfabrikken.vec.src.server.controller.log;

import no.vitenfabrikken.vec.src.library.utils.logger.Log;
import no.vitenfabrikken.vec.src.server.VECServer;

import java.util.logging.Level;

public class ServerLogger extends Log {
    private static final Level CONSOLE_LOG_LEVEL = Level.OFF;
    private static final Level FILE_LOG_LEVEL = Level.SEVERE;
    private static final String LOG_FILE_DESTINATION = VECServer.DATA_FOLDER_PATH;
    private static ServerLogger instance;

    private ServerLogger() {
        super(CONSOLE_LOG_LEVEL, FILE_LOG_LEVEL, LOG_FILE_DESTINATION, ServerLogger.class.getName());
    }

    public static void logMessage(Level level, String message) {
        getInstance().writeMessage(level, message);
    }

    public static void logStackTrace(Level level, StackTraceElement[] stackTrace) {
        getInstance().writeStackTrace(level, stackTrace);
    }

    private static ServerLogger getInstance() {
        if (instance == null) {
            instance = new ServerLogger();
        }

        return instance;
    }
}
