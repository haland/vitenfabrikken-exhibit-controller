package no.vitenfabrikken.vec.src.server.model.tree;

import no.vitenfabrikken.vec.src.server.model.machine.Machine;

import javax.swing.tree.DefaultMutableTreeNode;

public class MachineTreeNode extends DefaultMutableTreeNode {
    private static final long serialVersionUID = -7520305926681797278L;

    private Machine machine;

    public MachineTreeNode(Machine machine) {
        super(machine);
        this.machine = machine;
    }

    public Machine getMachine() {
        return machine;
    }
}
