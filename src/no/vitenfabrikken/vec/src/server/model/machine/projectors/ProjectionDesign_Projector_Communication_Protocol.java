package no.vitenfabrikken.vec.src.server.model.machine.projectors;


import no.vitenfabrikken.vec.src.server.controller.log.ServerLogger;
import no.vitenfabrikken.vec.src.server.model.machine.Projector;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.logging.Level;

@XmlRootElement(name = "projectiondesign-projector-communication-protocol")
public class ProjectionDesign_Projector_Communication_Protocol extends Projector {
    private static final int DEFAULT_PORT = 1025;
    private static final int TIMEOUT = 200;

    public ProjectionDesign_Projector_Communication_Protocol() {
        super();
    }

    public ProjectionDesign_Projector_Communication_Protocol(String ip, int port, String name) {
        super(ip, port, name);
    }

    @Override
    public void refresh() {
        byte[] POWER = {0x3a, 0x50, 0x4f, 0x53, 0x54, 0x3f, 0x0d};
        byte[] ack1 = sendCommand(POWER);

        int powerState = getValue(ack1);

        if (powerState == 2) {
            setStatus(resourceBundle.getString("PROJECTOR_POWERING_ON"));
        } else if (powerState == 3) {
            setStatus(resourceBundle.getString("PROJECTOR_ON"));
        } else if (powerState == 4) {
            setStatus(resourceBundle.getString("PROJECTOR_POWERING_OFF"));
        } else {
            setStatus(resourceBundle.getString("PROJECTOR_OFF"));
        }

        byte[] LAMP_TIME = {0x3a, 0x4c, 0x54, 0x52, 0x31, 0x3f, 0x0d};
        byte[] ack2 = sendCommand(LAMP_TIME);

        setLampHours(getValue(ack2));
        setFilterHours(Projector.FILTER_HOURS_NOT_AVAILABLE);

        byte[] LAMP_STATUS = {0x3a, 0x4c, 0x53, 0x54, 0x31, 0x3f, 0x0d};
        byte[] ack3 = sendCommand(LAMP_STATUS);

        byte[] STATUS = {0x3a, 0x4c, 0x45, 0x53, 0x54, 0x3f, 0x0d};
        byte[] ack4 = sendCommand(STATUS);

        int lampState = getValue(ack3);
        int statusState = getValue(ack4);

        if (lampState == 0) {
            setError(resourceBundle.getString("PROJECTOR_LAMP_ERROR"));
        } else if (lampState == 5) {
            setError(resourceBundle.getString("PROJECTOR_NO_LAMP"));
        } else if (statusState == 4) {
            setError(resourceBundle.getString("PROJECTOR_LAMP_EOL"));
        } else if (statusState == 5) {
            setError(resourceBundle.getString("PROJECTOR_TEMPERATURE_ERROR"));
        } else {
            setError(resourceBundle.getString("PROJECTOR_NO_ERROR"));
        }
    }

    @Override
    public int getDefaultPort() {
        return DEFAULT_PORT;
    }

    @Override
    public void wake() {
        byte[] POWER_ON = {0x3a, 0x50, 0x4f, 0x57, 0x52, 0x31, 0x0d};
        sendCommand(POWER_ON);
    }

    @Override
    public void shutdown() {
        byte[] POWER_OFF = {0x3a, 0x50, 0x4f, 0x57, 0x52, 0x30, 0x0d};
        sendCommand(POWER_OFF);
    }

    private synchronized byte[] sendCommand(byte[] bytes) {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
        }

        byte[] ack = null;
        Socket socket = null;
        InputStream in = null;
        OutputStream out = null;

        try {
            socket = new Socket();
            socket.connect(new InetSocketAddress(ip, port), TIMEOUT);
            in = socket.getInputStream();
            out = socket.getOutputStream();

            out.flush();
            out.write(bytes);

            byte[] buffer = new byte[2048];
            int size = in.read(buffer);

            if (size > 0) {
                ack = new byte[size];
                System.arraycopy(buffer, 0, ack, 0, size);
            }
        } catch (NullPointerException e) {
            setError(resourceBundle.getString("PROJECTOR_NO_CONNECTION"));
            setStatus(resourceBundle.getString("PROJECTOR_OFF"));
        } catch (IOException e) {
            setError(resourceBundle.getString("PROJECTOR_NO_CONNECTION"));
            setStatus(resourceBundle.getString("PROJECTOR_OFF"));
        } finally {
            try {
                if (in != null)
                    in.close();
                if (out != null)
                    out.close();
                if (socket != null)
                    socket.close();
            } catch (IOException e) {
                ServerLogger.logMessage(Level.FINEST, "Network streams already closed: " + e.getLocalizedMessage());
                e.printStackTrace();
            }
        }

        return ack;
    }

    private int getValue(byte[] response) {
        byte[] byteValue = new byte[6];
        System.arraycopy(response, response.length - 9, byteValue, 0, 6);

        String textValue = new String(byteValue);

        return Integer.parseInt(textValue);
    }
}
