package no.vitenfabrikken.vec.src.server.model.machine;

import no.vitenfabrikken.vec.src.library.utils.locale.LocaleResource;
import no.vitenfabrikken.vec.src.server.model.machine.listeners.MachineSettingsChangedEvent;
import no.vitenfabrikken.vec.src.server.model.machine.projectors.Epson_Projector_Communication_Protocol;
import no.vitenfabrikken.vec.src.server.model.machine.projectors.NEC_Projector_Communication_Protocol;
import no.vitenfabrikken.vec.src.server.model.machine.projectors.ProjectionDesign_Projector_Communication_Protocol;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.ResourceBundle;

@XmlRootElement(name = "projector")
@XmlSeeAlso({NEC_Projector_Communication_Protocol.class, Epson_Projector_Communication_Protocol.class,
        ProjectionDesign_Projector_Communication_Protocol.class})
public abstract class Projector extends Machine {
    public static final int LAMP_HOURS_NOT_AVAILABLE = Integer.MIN_VALUE;
    public static final int FILTER_HOURS_NOT_AVAILABLE = Integer.MIN_VALUE;

    protected int port;
    protected int lampHours;
    protected int filterHours;
    protected ResourceBundle resourceBundle;

    public Projector() {
        super();
        this.resourceBundle = LocaleResource.getLocaleResourceBundle();
    }

    public Projector(String ip, int port, String name) {
        super(ip, name);

        this.port = port;
        this.resourceBundle = LocaleResource.getLocaleResourceBundle();
    }

    protected static String byteToHex(byte... bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(String.format("%02X", b));
        }

        return sb.toString();
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        if (port != this.port) {
            Integer oldPort = new Integer(this.port);

            this.port = port;
            notifySettingsChangedListeners(new MachineSettingsChangedEvent(MachineSettingsChangedEvent.STATUS,
                    oldPort, this));
        }
    }

    public int getLampHours() {
        return lampHours;
    }

    public void setLampHours(int lampHours) {
        if (lampHours != this.lampHours) {
            Integer oldLampHours = new Integer(this.lampHours);

            this.lampHours = lampHours;
            notifySettingsChangedListeners(new MachineSettingsChangedEvent(MachineSettingsChangedEvent.LAMP_HOURS,
                    oldLampHours, this));
        }
    }

    public int getFilterHours() {
        return filterHours;
    }

    public void setFilterHours(int filterHours) {
        if (filterHours != this.filterHours) {
            Integer oldFilterHours = new Integer(this.filterHours);

            this.filterHours = filterHours;
            notifySettingsChangedListeners(new MachineSettingsChangedEvent(
                    MachineSettingsChangedEvent.FILTER_HOURS, oldFilterHours, this));
        }

    }

    @Override
    public void restart() {
    }

    public abstract void refresh();

    public abstract int getDefaultPort();
}
