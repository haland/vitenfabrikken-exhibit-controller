package no.vitenfabrikken.vec.src.server.model.machine.projectors;

import no.vitenfabrikken.vec.src.server.model.machine.Projector;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Arrays;

@XmlRootElement(name = "epson-projector-communication-protocol")
public class Epson_Projector_Communication_Protocol extends Projector {
    private static final int TIMEOUT = 200;
    private static final int DEFAULT_PORT = 3629;
    private static final byte[] UNSUPPORTED_COMMAND = {0x45, 0x52, 0x52, 0x0d, 0x3a};

    public Epson_Projector_Communication_Protocol() {
        super();
    }

    public Epson_Projector_Communication_Protocol(String ip, int port, String name) {
        super(ip, port, name);
    }

    @Override
    public void refresh() {
        byte[] POWER = {0x50, 0x57, 0x52, 0x3f, 0x0d};
        byte[] ack1 = sendCommand(POWER);

        if (ack1 != null && !Arrays.equals(ack1, UNSUPPORTED_COMMAND)) {
            try {
                byte[] returnBytes = new byte[ack1.length - 6];
                System.arraycopy(ack1, 4, returnBytes, 0, returnBytes.length);
                int status = Integer.parseInt(new String(returnBytes));

                if (status == 1)
                    setStatus(resourceBundle.getString("PROJECTOR_ON"));
                else if (status == 2)
                    setStatus(resourceBundle.getString("PROJECTOR_POWERING_ON"));
                else if (status == 3)
                    setStatus(resourceBundle.getString("PROJECTOR_POWERING_OFF"));
                else if (status == 0 || status == 4 || status == 5)
                    setStatus(resourceBundle.getString("PROJECTOR_OFF"));
            } catch (NumberFormatException e) {
            }
        }

        byte[] LAMP = {0x4c, 0x41, 0x4d, 0x50, 0x3f, 0x0d};
        byte[] ack2 = sendCommand(LAMP);

        if (ack2 != null && !Arrays.equals(ack2, UNSUPPORTED_COMMAND)) {
            try {
                byte[] lampBytes = new byte[ack2.length - 7];
                System.arraycopy(ack2, 5, lampBytes, 0, lampBytes.length);
                int lampHours = Integer.parseInt(new String(lampBytes));

                setLampHours(lampHours);
            } catch (NumberFormatException e) {
            }
        }

        byte[] FILTER = {0x46, 0x49, 0x4c, 0x54, 0x45, 0x52, 0x3f, 0x0d};
        byte[] ack3 = sendCommand(FILTER);

        if (ack3 != null && !Arrays.equals(ack3, UNSUPPORTED_COMMAND)) {
            try {
                byte[] filterBytes = new byte[ack3.length - 9];
                System.arraycopy(ack3, 7, filterBytes, 0, filterBytes.length);
                int filterHours = Integer.parseInt(new String(filterBytes));

                setFilterHours(filterHours);
            } catch (NumberFormatException e) {
            }
        } else {
            setFilterHours(Projector.FILTER_HOURS_NOT_AVAILABLE);
        }

        //Error check has not been tested. This may be implemented wrongly.
        byte[] ERROR = {0x45, 0x52, 0x52, 0x3f, 0x0d};
        byte[] ack4 = sendCommand(ERROR);

        if (ack4 != null && !Arrays.equals(ack4, UNSUPPORTED_COMMAND)) {
            byte[] returnBytes = new byte[ack4.length - 6];
            System.arraycopy(ack4, 4, returnBytes, 0, returnBytes.length);
            String error = new String(returnBytes);

            if (error.equals("00")) { //There is no error or the error is recovered
                setError(resourceBundle.getString("PROJECTOR_NO_ERROR"));
            } else if (error.equals("01")) { //Fan error
                setError(resourceBundle.getString("PROJECTOR_FAN_ERROR"));
            } else if (error.equals("03")) { //Lamp failure at power on
                setError(resourceBundle.getString("PROJECTOR_LAMP_ERROR"));
            } else if (error.equals("04")) { //High internal temperature error
                setError(resourceBundle.getString("PROJECTOR_TEMPERATURE_ERROR"));
            } else if (error.equals("06")) { //Lamp error
                setError(resourceBundle.getString("PROJECTOR_LAMP_ERROR"));
            } else if (error.equals("07")) { //Open lamp cover door error
                setError(resourceBundle.getString("PROJECTOR_LAMP_COVER_ERROR"));
            } else if (error.equals("08")) { //Cinema filter error
                setError(resourceBundle.getString("PROJECTOR_ERROR_CODE") + "08");
            } else if (error.equals("09")) { //Electric dual-layered capacitor is disconnected
                setError(resourceBundle.getString("PROJECTOR_ERROR_CODE") + "09");
            } else if (error.equals("0A")) { //Auto iris error
                setError(resourceBundle.getString("PROJECTOR_ERROR_CODE") + "0A");
            } else if (error.equals("0B")) { //Subsystem error
                setError(resourceBundle.getString("PROJECTOR_ERROR_CODE") + "0B");
            } else if (error.equals("0C")) { //Low air flow error
                setError(resourceBundle.getString("PROJECTOR_ERROR_CODE") + "0C");
            } else if (error.equals("0D")) { //Air filter air flow sensor error
                setError(resourceBundle.getString("PROJECTOR_ERROR_CODE") + "0D");
            } else if (error.equals("0E")) { //Power supply unit error (Ballast)
                setError(resourceBundle.getString("PROJECTOR_POWER_ERROR"));
            } else if (error.equals("0F")) { //Shutter error
                setError(resourceBundle.getString("PROJECTOR_ERROR_CODE") + "0E");
            } else if (error.equals("10")) { //Cooling system error (peltiert element)
                setError(resourceBundle.getString("PROJECTOR_ERROR_CODE") + "10");
            } else if (error.equals("11")) { //Cooling system error (Pump)
                setError(resourceBundle.getString("PROJECTOR_ERROR_CODE") + "11");
            }
        } else {
            setError(resourceBundle.getString("ERROR_NOT_AVAILABLE"));
        }
    }

    @Override
    public void wake() {
        byte[] WAKE_OK = {0x3a};
        byte[] WAKE = {0x50, 0x57, 0x52, 0x20, 0x4f, 0x4e, 0x0d};
        byte[] ack = sendCommand(WAKE);

        if (Arrays.equals(ack, WAKE_OK))
            setStatus(resourceBundle.getString("PROJECTOR_POWERING_ON"));
    }

    @Override
    public void shutdown() {
        byte[] SHUTDOWN_OK = {0x3a};
        byte[] SHUTDOWN = {0x50, 0x57, 0x52, 0x20, 0x4f, 0x46, 0x46, 0x0d};
        byte[] ack = sendCommand(SHUTDOWN);

        if (Arrays.equals(ack, SHUTDOWN_OK))
            setStatus(resourceBundle.getString("PROJECTOR_POWERING_OFF"));
    }

    @Override
    public int getDefaultPort() {
        return DEFAULT_PORT;
    }

    private synchronized byte[] sendCommand(byte[] bytes) {
        byte[] CONNECT = {0x45, 0x53, 0x43, 0x2f, 0x56, 0x50, 0x2e, 0x6e, 0x65, 0x74, 0x10, 0x03, 0x00,
                0x00, 0x00, 0x00};

        byte[] ack = null;
        byte[] conn = null;
        Socket socket = null;
        InputStream in = null;
        OutputStream out = null;

        try {
            socket = new Socket();
            socket.connect(new InetSocketAddress(ip, port), TIMEOUT);
            in = socket.getInputStream();
            out = socket.getOutputStream();

            out.flush();
            out.write(CONNECT);

            byte[] connBuffer = new byte[2048];
            int connSize = in.read(connBuffer);

            if (connSize > 0) {
                conn = new byte[connSize];
                System.arraycopy(connBuffer, 0, conn, 0, connSize);

                if (conn[14] == 0x20) { // Connection response is OK
                    out.flush();
                    out.write(bytes);
                    out.flush();
                    byte[] ackBuffer = new byte[2048];
                    int ackSize = in.read(ackBuffer);

                    if (ackSize > 0) {
                        ack = new byte[ackSize];
                        System.arraycopy(ackBuffer, 0, ack, 0, ackSize);
                    }
                }
            }
        } catch (NullPointerException e) {
            setStatus(resourceBundle.getString("PROJECTOR_NO_CONNECTION"));
        } catch (IOException e) {
            setStatus(resourceBundle.getString("PROJECTOR_NO_CONNECTION"));
        } finally {
            try {
                if (in != null)
                    in.close();
                if (out != null)
                    out.close();
                if (socket != null)
                    socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return ack;
    }
}