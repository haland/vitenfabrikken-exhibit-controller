package no.vitenfabrikken.vec.src.server.model.machine;

import no.vitenfabrikken.vec.src.library.utils.locale.LocaleResource;
import no.vitenfabrikken.vec.src.server.model.machine.listeners.MachineSettingsChangedEvent;
import no.vitenfabrikken.vec.src.server.model.machine.listeners.MachineSettingsChangedListener;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.Vector;

@XmlRootElement(name = "machine")
@XmlSeeAlso({Projector.class, Computer.class})
public abstract class Machine {
    protected String ip;
    protected String name;
    protected String error;
    protected String status;

    private ResourceBundle resourceBundle;
    private Vector<MachineSettingsChangedListener> listeners;

    public Machine() {
        super();
        resourceBundle = LocaleResource.getLocaleResourceBundle();
        listeners = new Vector<MachineSettingsChangedListener>();
    }

    public Machine(String ip, String name) {
        this.ip = ip;
        this.name = name;

        resourceBundle = LocaleResource.getLocaleResourceBundle();
        listeners = new Vector<MachineSettingsChangedListener>();
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        if (!ip.equals(this.ip)) {
            String oldIp = this.ip;

            this.ip = ip;
            notifySettingsChangedListeners(new MachineSettingsChangedEvent(MachineSettingsChangedEvent.IP,
                    oldIp, this));
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (!name.equals(this.name)) {
            String oldName = this.name;

            this.name = name;
            notifySettingsChangedListeners(new MachineSettingsChangedEvent(MachineSettingsChangedEvent.NAME,
                    oldName, this));
        }
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        if (!status.equals(this.status)) {
            String oldStatus = this.status;

            this.status = status;
            notifySettingsChangedListeners(new MachineSettingsChangedEvent(MachineSettingsChangedEvent.STATUS,
                    oldStatus, this));
        }
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        if (!error.equals(this.error)) {
            String oldError = this.error;

            this.error = error;
            notifySettingsChangedListeners(new MachineSettingsChangedEvent(MachineSettingsChangedEvent.ERROR,
                    oldError, this));
        }
    }

    protected void notifySettingsChangedListeners(MachineSettingsChangedEvent e) {
        Iterator<MachineSettingsChangedListener> i = listeners.iterator();
        while (i.hasNext())
            i.next().settingsChange(e);
    }

    public void addSettingsChangedListener(MachineSettingsChangedListener s) {
        if (!listeners.contains(s))
            listeners.add(s);
    }

    public void removeSettingsChangedListener(MachineSettingsChangedListener s) {
        if (listeners.contains(s))
            listeners.remove(s);
    }

    @Override
    public String toString() {
        return name + (error == null ?
                ((status != null && !status.equals("")) ? resourceBundle.getString("MACHINE_STATUS_PART_1") + status +
                        resourceBundle.getString("MACHINE_STATUS_PART_3") : "") :
                ((status != null && !status.equals("")) ?
                        resourceBundle.getString("MACHINE_STATUS_PART_1") + status +
                                resourceBundle.getString("MACHINE_STATUS_PART_2") + error +
                                resourceBundle.getString("MACHINE_STATUS_PART_3") : ""));
    }

    public abstract void wake();

    public abstract void restart();

    public abstract void shutdown();
}
