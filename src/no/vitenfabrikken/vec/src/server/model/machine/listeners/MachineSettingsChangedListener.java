package no.vitenfabrikken.vec.src.server.model.machine.listeners;

public interface MachineSettingsChangedListener {
    public void settingsChange(MachineSettingsChangedEvent e);
}
