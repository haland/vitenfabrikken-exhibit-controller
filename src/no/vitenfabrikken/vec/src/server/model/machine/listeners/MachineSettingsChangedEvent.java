package no.vitenfabrikken.vec.src.server.model.machine.listeners;

import no.vitenfabrikken.vec.src.server.model.machine.Machine;

public class MachineSettingsChangedEvent {
    public static final int IP = 0x00;
    public static final int MAC = 0x01;
    public static final int NAME = 0x02;
    public static final int ERROR = 0x03;
    public static final int STATUS = 0x04;
    public static final int USERNAME = 0x05;
    public static final int PASSWORD = 0x06;
    public static final int LAMP_HOURS = 0x07;
    public static final int FILTER_HOURS = 0x08;

    private int type;
    private Object oldValue;
    private Machine machine;

    public MachineSettingsChangedEvent(int type, Object oldValue, Machine machine) {
        this.type = type;
        this.machine = machine;
        this.oldValue = oldValue;
    }

    public int getType() {
        return type;
    }

    public Object getOldValue() {
        return oldValue;
    }

    public Machine getSource() {
        return machine;
    }
}
