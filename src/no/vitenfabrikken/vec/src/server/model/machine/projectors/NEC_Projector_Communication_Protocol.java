package no.vitenfabrikken.vec.src.server.model.machine.projectors;

import no.vitenfabrikken.vec.src.server.controller.log.ServerLogger;
import no.vitenfabrikken.vec.src.server.model.machine.Projector;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.logging.Level;

@XmlRootElement(name = "nec-projector-communication-protocol")
public class NEC_Projector_Communication_Protocol extends Projector {
    private static final int DEFAULT_PORT = 7142;
    private static final int TIMEOUT = 200;
    private boolean poweringDown = false;

    public NEC_Projector_Communication_Protocol() {
        super();
    }

    public NEC_Projector_Communication_Protocol(String ip, int port, String name) {
        super(ip, port, name);
    }

    private static int[] toBinary(int integer) {
        int[] bits = new int[8];
        for (int i = bits.length - 1; i >= 0; i--)
            bits[i] = ((integer & (1 << i)) != 0) ? 1 : 0;
        return bits;
    }

    @Override
    public void wake() {
        poweringDown = false;
        byte[] wakeup = {0x02, 0x00, 0x00, 0x00, 0x00, 0x02};
        sendCommand(wakeup);
    }

    @Override
    public void shutdown() {
        poweringDown = true;
        byte[] shutdown = {0x02, 0x01, 0x00, 0x00, 0x00, 0x03};
        sendCommand(shutdown);
    }

    @Override
    public void refresh() {
        try {
            //RUNNING SENSE
            String status = "";
            byte[] runningSense = {0x00, (byte) 0x81, 0x00, 0x00, 0x00, (byte) 0x81};
            byte[] ack1 = sendCommand(runningSense);
            int[] bits1 = toBinary((int) ack1[5]);

            if (bits1[1] == 1) { //Projector status
                status = resourceBundle.getString("PROJECTOR_ON");
            } else status = resourceBundle.getString("PROJECTOR_OFF");

            if (bits1[5] == 1) { //Cooling processing
                status = resourceBundle.getString("PROJECTOR_COOLING_DOWN");
            }

            if (bits1[7] == 1 && bits1[5] == 1) { //Powering off and cooling down
                status = resourceBundle.getString("PROJECTOR_POWERING_OFF");
            }

            if (bits1[7] == 1 && !poweringDown) { //Power on/off processing
                status = resourceBundle.getString("PROJECTOR_POWERING_ON");
            }

            if (!status.equals("")) {
                if (bits1[3] == 1) //Can�t process "power off" command
                    status += " (" + resourceBundle.getString("PROJECTOR_CANT_POWER_OFF") + ")";

                setStatus(status);
            }

            //ERROR STATUS REQUEST
            String error = "";
            byte[] errorStatus = {0x00, (byte) 0x88, 0x00, 0x00, 0x00, (byte) 0x88};
            byte[] ack2 = sendCommand(errorStatus);
            int[] bits2 = toBinary((int) ack2[5]);

            if (bits2[0] == 1) { //Lamp cover error
                error += ", " + resourceBundle.getString("PROJECTOR_LAMP_COVER_ERROR");
            }

            if (bits2[1] == 1) { //Temperature error
                error += ", " + resourceBundle.getString("PROJECTOR_TEMPERATURE_ERROR");
            }

            if (bits2[3] == 1 || bits2[4] == 1) { //Fan error
                error += ", " + resourceBundle.getString("PROJECTOR_FAN_ERROR");
            }

            if (bits2[5] == 1) { //Power error
                error += ", " + resourceBundle.getString("PROJECTOR_POWER_ERROR");
            }

            if (bits2[6] == 1) { //Lamp error
                error += ", " + resourceBundle.getString("PROJECTOR_LAMP_ERROR");
            }

            if (bits2[7] == 1) { //Lamp has reached its end of life
                error += ", " + resourceBundle.getString("PROJECTOR_LAMP_EOL");
            }

            if (error.startsWith(","))
                error.replaceFirst(", ", "");
            else error = resourceBundle.getString("PROJECTOR_NO_ERROR");

            setError(error);

            //LAMP INFORMATION REQUEST
            byte[] lampInformation = {0x03, (byte) 0x8C, 0x00, 0x00, 0x00, (byte) 0x8F};
            byte[] ack3 = sendCommand(lampInformation);

            String hex1 = byteToHex(ack3[8], ack3[7], ack3[6], ack3[5]);
            int lampSeconds = Integer.parseInt(hex1, 16);

            setLampHours(lampSeconds / 3600);

            //FILTER INFORMATION REQUEST
            byte[] filterInformation = {0x03, (byte) 0x95, 0x00, 0x00, 0x00, (byte) 0x98};
            byte[] ack4 = sendCommand(filterInformation);

            String hex2 = byteToHex(ack4[8], ack4[7], ack4[6], ack4[5]);
            int filterSeconds = Integer.parseInt(hex2, 16);

            setFilterHours(filterSeconds / 3600);
        } catch (NullPointerException e) {
            setError(resourceBundle.getString("PROJECTOR_NO_CONNECTION"));
            setStatus(resourceBundle.getString("PROJECTOR_OFF"));
        }
    }

    @Override
    public int getDefaultPort() {
        return DEFAULT_PORT;
    }

    private synchronized byte[] sendCommand(byte[] bytes) {
        byte[] ack = null;
        Socket socket = null;
        InputStream in = null;
        OutputStream out = null;

        try {
            socket = new Socket();
            socket.connect(new InetSocketAddress(ip, port), TIMEOUT);
            in = socket.getInputStream();
            out = socket.getOutputStream();

            out.flush();
            out.write(bytes);

            byte[] buffer = new byte[2048];
            int size = in.read(buffer);

            if (size > 0) {
                ack = new byte[size];
                System.arraycopy(buffer, 0, ack, 0, size);
            }
        } catch (NullPointerException e) {
            setError(resourceBundle.getString("PROJECTOR_NO_CONNECTION"));
            setStatus(resourceBundle.getString("PROJECTOR_OFF"));
        } catch (IOException e) {
            setError(resourceBundle.getString("PROJECTOR_NO_CONNECTION"));
            setStatus(resourceBundle.getString("PROJECTOR_OFF"));
        } finally {
            try {
                if (in != null)
                    in.close();
                if (out != null)
                    out.close();
                if (socket != null)
                    socket.close();
            } catch (IOException e) {
                ServerLogger.logMessage(Level.FINEST, "Network streams already closed: " + e.getLocalizedMessage());
                e.printStackTrace();
            }
        }

        return ack;
    }
}
