package no.vitenfabrikken.vec.src.server.model.machine;

import no.vitenfabrikken.vec.src.library.msg.Command;
import no.vitenfabrikken.vec.src.library.utils.network.TCPSocket;
import no.vitenfabrikken.vec.src.library.utils.network.card.MACAddress;
import no.vitenfabrikken.vec.src.library.utils.network.card.exceptions.IllegalMACAddressException;
import no.vitenfabrikken.vec.src.library.utils.network.wakeonlan.WakeOnLan;
import no.vitenfabrikken.vec.src.server.VECServer;
import no.vitenfabrikken.vec.src.server.controller.log.ServerLogger;
import no.vitenfabrikken.vec.src.server.model.machine.listeners.MachineSettingsChangedEvent;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.IOException;
import java.net.ConnectException;
import java.util.logging.Level;

@XmlRootElement(name = "computer")
public class Computer extends Machine {
    private String mac;
    private long lastUpdatedStatusAt;

    public Computer() {
        super();
        lastUpdatedStatusAt = System.currentTimeMillis();
    }

    public Computer(String ip, String mac, String name) {
        super(ip, name);
        this.mac = mac;
    }

    @Override
    public void wake() {
        try {
            WakeOnLan.wakeup(new MACAddress(mac));
        } catch (IOException e) {
            ServerLogger.logMessage(Level.SEVERE, "Could not send Wake on LAN signal: " + e.getLocalizedMessage());
            e.printStackTrace();
        } catch (IllegalMACAddressException e) {
            ServerLogger.logMessage(Level.WARNING, "MAC address is not formatted correctly: " + e.getLocalizedMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void restart() {
        try {
            TCPSocket socket = VECServer.getInstance().getNetworkHandler().getSocket();
            socket.send(ip, new Command(Command.RESTART));
        } catch (ConnectException e) {
            ServerLogger.logMessage(Level.SEVERE, "Could not send restart signal to '" + name + "': " +
                    e.getLocalizedMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void shutdown() {
        try {
            TCPSocket socket = VECServer.getInstance().getNetworkHandler().getSocket();
            socket.send(ip, new Command(Command.SHUTDOWN));
        } catch (ConnectException e) {
            ServerLogger.logMessage(Level.SEVERE, "Could not send shutdown signal to '" + name + "': " +
                    e.getLocalizedMessage());
            e.printStackTrace();
        }
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        if (!mac.equals(this.mac)) {
            String oldMac = this.mac;

            this.mac = mac;
            notifySettingsChangedListeners(new MachineSettingsChangedEvent(MachineSettingsChangedEvent.MAC,
                    oldMac, this));
        }
    }

    public long getLastUpdatedStatusAt() {
        return lastUpdatedStatusAt;
    }

    public void setLastUpdatedStatusAt(long lastUpdatedStatusAt) {
        this.lastUpdatedStatusAt = lastUpdatedStatusAt;
    }
}
