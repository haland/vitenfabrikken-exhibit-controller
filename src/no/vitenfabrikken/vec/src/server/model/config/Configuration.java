package no.vitenfabrikken.vec.src.server.model.config;

import no.vitenfabrikken.vec.src.library.utils.file.ObjectXMLReader;
import no.vitenfabrikken.vec.src.library.utils.file.ObjectXMLWriter;
import no.vitenfabrikken.vec.src.server.VECServer;
import no.vitenfabrikken.vec.src.server.model.config.listeners.ConfigurationChangedEvent;
import no.vitenfabrikken.vec.src.server.model.config.listeners.ConfigurationChangedListener;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.File;
import java.util.Iterator;
import java.util.Vector;

@XmlRootElement(name = "server-config")
public class Configuration {
    public static final String CONFIG_FILE_PATH = VECServer.DATA_FOLDER_PATH + File.separator + "config" +
            VECServer.FILE_TYPE;
    private static Configuration instance = null;
    private int port;
    private String alwaysOnlineText;
    private Vector<ConfigurationChangedListener> listeners;

    public Configuration() {
        listeners = new Vector<ConfigurationChangedListener>();
    }

    public Configuration(int port, String alwaysOnlineText) {
        this.port = port;
        this.alwaysOnlineText = alwaysOnlineText;
        this.listeners = new Vector<ConfigurationChangedListener>();
    }

    public static Configuration getCurrentConfiguration() {
        if (instance == null) {
            instance = (Configuration) ObjectXMLReader.read(Configuration.class, new File(CONFIG_FILE_PATH).toPath());
        }

        return instance;
    }

    public static boolean configExists() {
        return new File(CONFIG_FILE_PATH).exists();
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        if (port != this.port) {
            this.port = port;
            notifyConfigurationChangedListeners(new ConfigurationChangedEvent(this,
                    ConfigurationChangedEvent.PORT_CHANGED));
        }
    }

    public String getAlwaysOnlineText() {
        return alwaysOnlineText;
    }

    public void setAlwaysOnlineText(String alwaysOnlineText) {
        if (!alwaysOnlineText.equals(this.alwaysOnlineText)) {
            this.alwaysOnlineText = alwaysOnlineText;
            notifyConfigurationChangedListeners(new ConfigurationChangedEvent(this,
                    ConfigurationChangedEvent.ALWAYS_ONLINE_TEXT_CHANGED));
        }
    }

    public void save() {
        File f = new File(CONFIG_FILE_PATH);
        ObjectXMLWriter.write(Configuration.class, this, f);
    }

    private void notifyConfigurationChangedListeners(ConfigurationChangedEvent e) {
        Iterator<ConfigurationChangedListener> i = listeners.iterator();
        while (i.hasNext())
            i.next().configChanged(e);
    }

    public void addConfigurationChangedListener(ConfigurationChangedListener l) {
        if (!listeners.contains(l))
            listeners.add(l);
    }

    public void removeConfigurationChangedListener(ConfigurationChangedListener l) {
        if (listeners.contains(l))
            listeners.remove(l);
    }
}
