package no.vitenfabrikken.vec.src.server.model.config.listeners;

public interface ConfigurationChangedListener {
    public void configChanged(ConfigurationChangedEvent e);
}
