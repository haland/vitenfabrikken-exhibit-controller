package no.vitenfabrikken.vec.src.server.model.config.listeners;

import no.vitenfabrikken.vec.src.server.model.config.Configuration;

public class ConfigurationChangedEvent {
    public static final int PORT_CHANGED = 0;
    public static final int ALWAYS_ONLINE_TEXT_CHANGED = 1;

    private int propertyChangedId;
    private Configuration config;

    public ConfigurationChangedEvent(Configuration config, int propertyChangedId) {
        this.config = config;
        this.propertyChangedId = propertyChangedId;
    }

    public int getChangedPropertyId() {
        return propertyChangedId;
    }

    public Configuration getSource() {
        return config;
    }
}
