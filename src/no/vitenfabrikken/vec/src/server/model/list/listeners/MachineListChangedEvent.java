package no.vitenfabrikken.vec.src.server.model.list.listeners;

import no.vitenfabrikken.vec.src.server.model.machine.Machine;

public class MachineListChangedEvent {
    public static final int ADDED = 0x00;
    public static final int REMOVED = 0x01;

    private int status;
    private Machine machine;

    public MachineListChangedEvent(int status, Machine machine) {
        this.machine = machine;
    }

    public int getType() {
        return status;
    }

    public Machine getSource() {
        return machine;
    }
}
