package no.vitenfabrikken.vec.src.server.model.list;

import no.vitenfabrikken.vec.src.server.model.list.listeners.MachineListChangedEvent;
import no.vitenfabrikken.vec.src.server.model.list.listeners.MachineListChangedListener;
import no.vitenfabrikken.vec.src.server.model.machine.Computer;
import no.vitenfabrikken.vec.src.server.model.machine.Machine;
import no.vitenfabrikken.vec.src.server.model.machine.Projector;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;


public class MachineList extends ArrayList<Machine> {
    private static final long serialVersionUID = 7213831142793327211L;

    private Vector<MachineListChangedListener> listeners;

    public MachineList() {
        super();
        listeners = new Vector<MachineListChangedListener>();
    }

    public boolean add(Machine m) {
        boolean added = super.add(m);
        notifyListChanged(new MachineListChangedEvent(MachineListChangedEvent.ADDED, m));
        return added;
    }

    public Machine remove(int index) {
        Machine m = super.remove(index);
        notifyListChanged(new MachineListChangedEvent(MachineListChangedEvent.REMOVED, m));
        return m;
    }

    public boolean remove(Machine o) {
        boolean removed = super.remove(o);
        notifyListChanged(new MachineListChangedEvent(MachineListChangedEvent.REMOVED, o));
        return removed;
    }

    public Machine contains(String id) {
        for (Machine m : this) {
            if (m instanceof Computer) {
                if (((Computer) m).getMac().equals(id)) {
                    return m;
                }
            } else if (m instanceof Projector) {
                if (m.getIp().equals(id)) {
                    return m;
                }
            }
        }

        return null;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return super.toArray(a);
    }

    public void notifyListChanged(MachineListChangedEvent e) {
        Iterator<MachineListChangedListener> i = listeners.iterator();
        while (i.hasNext())
            i.next().machineListChanged(e);
    }

    public void addMachineListChangedListener(MachineListChangedListener l) {
        if (!listeners.contains(l))
            listeners.add(l);
    }

    public void removeMachineListChangedListener(MachineListChangedListener l) {
        if (listeners.contains(l))
            listeners.remove(l);
    }
}
