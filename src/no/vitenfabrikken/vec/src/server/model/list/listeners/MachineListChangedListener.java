package no.vitenfabrikken.vec.src.server.model.list.listeners;

public interface MachineListChangedListener {
    public void machineListChanged(MachineListChangedEvent e);
}
