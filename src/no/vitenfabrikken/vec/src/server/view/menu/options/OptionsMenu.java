package no.vitenfabrikken.vec.src.server.view.menu.options;

import no.vitenfabrikken.vec.src.library.utils.locale.LocaleResource;
import no.vitenfabrikken.vec.src.server.model.config.Configuration;
import no.vitenfabrikken.vec.src.server.view.dialog.settings.SettingsDialog;
import no.vitenfabrikken.vec.src.server.view.window.Window;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class OptionsMenu extends JMenu implements ActionListener {
    private static final long serialVersionUID = 6290452456560753498L;

    private Window window;
    private JMenuItem settings;
    private Configuration config;

    public OptionsMenu(Configuration config, Window window) {
        super(LocaleResource.getLocaleResourceBundle().getString("OPTIONS_MENU"));
        this.config = config;
        this.window = window;

        settings = new JMenuItem(LocaleResource.getLocaleResourceBundle().getString("OPTIONS_MENU_SETTINGS"));
        settings.addActionListener(this);

        add(settings);
    }

    public JMenuItem getSettingsMenuItem() {
        return settings;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        new SettingsDialog(config, window);
    }
}
