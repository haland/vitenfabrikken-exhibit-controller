package no.vitenfabrikken.vec.src.server.view.menu;

import no.vitenfabrikken.vec.src.server.model.config.Configuration;
import no.vitenfabrikken.vec.src.server.model.list.MachineList;
import no.vitenfabrikken.vec.src.server.view.menu.edit.EditMenu;
import no.vitenfabrikken.vec.src.server.view.menu.file.FileMenu;
import no.vitenfabrikken.vec.src.server.view.menu.options.OptionsMenu;
import no.vitenfabrikken.vec.src.server.view.window.Window;

import javax.swing.*;

public class MenuBar extends JMenuBar {
    private static final long serialVersionUID = -6301180590966544328L;

    private FileMenu fileMenu;
    private EditMenu editMenu;
    private OptionsMenu optionsMenu;

    public MenuBar(Configuration config, MachineList list, Window window) {
        super();

        fileMenu = new FileMenu();
        editMenu = new EditMenu(list);
        optionsMenu = new OptionsMenu(config, window);

        add(fileMenu);
        add(editMenu);
        add(optionsMenu);
    }

    public FileMenu getFileMenu() {
        return fileMenu;
    }

    public EditMenu getEditMenu() {
        return editMenu;
    }

    public OptionsMenu getOptionsMenu() {
        return optionsMenu;
    }
}
