package no.vitenfabrikken.vec.src.server.view.menu.edit;

import no.vitenfabrikken.vec.src.library.utils.locale.LocaleResource;
import no.vitenfabrikken.vec.src.server.model.list.MachineList;
import no.vitenfabrikken.vec.src.server.view.dialog.machine.projector.AddProjectorDialog;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class EditMenu extends JMenu implements ActionListener {
    private static final long serialVersionUID = 6313048704944243617L;

    private JMenuItem addProjector;
    private MachineList machineList;

    public EditMenu(MachineList machineList) {
        super(LocaleResource.getLocaleResourceBundle().getString("EDIT_MENU"));
        this.machineList = machineList;

        addProjector = new JMenuItem(LocaleResource.getLocaleResourceBundle().getString("EDIT_MENU_ADD_PROJECTOR"));
        addProjector.addActionListener(this);

        add(addProjector);
    }

    public JMenuItem getAddProjectorMenuItem() {
        return addProjector;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        new AddProjectorDialog(machineList);
    }
}
