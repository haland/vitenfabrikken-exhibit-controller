package no.vitenfabrikken.vec.src.server.view.menu.file;

import no.vitenfabrikken.vec.src.library.utils.locale.LocaleResource;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FileMenu extends JMenu implements ActionListener {
    private static final long serialVersionUID = -645713714247733665L;

    private JMenuItem exit;

    public FileMenu() {
        super(LocaleResource.getLocaleResourceBundle().getString("FILE_MENU"));

        exit = new JMenuItem(LocaleResource.getLocaleResourceBundle().getString("FILE_MENU_EXIT"));
        exit.addActionListener(this);

        add(exit);
    }

    public JMenuItem getExitMenuItem() {
        return exit;
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        System.exit(0);
    }
}
