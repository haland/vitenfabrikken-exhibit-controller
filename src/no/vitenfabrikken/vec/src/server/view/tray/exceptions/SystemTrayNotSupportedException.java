package no.vitenfabrikken.vec.src.server.view.tray.exceptions;

public class SystemTrayNotSupportedException extends Exception {
    private static final long serialVersionUID = -3116379988166769333L;
}
