package no.vitenfabrikken.vec.src.server.view.tray;

import no.vitenfabrikken.vec.src.library.utils.locale.LocaleResource;
import no.vitenfabrikken.vec.src.server.VECServer;
import no.vitenfabrikken.vec.src.server.model.config.Configuration;
import no.vitenfabrikken.vec.src.server.view.dialog.settings.SettingsDialog;
import no.vitenfabrikken.vec.src.server.view.tray.exceptions.SystemTrayNotSupportedException;
import no.vitenfabrikken.vec.src.server.view.window.Window;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ResourceBundle;


public class Tray implements MouseListener, ActionListener {
    private static final int FONT_SIZE = 12;
    private static final String FONT = "Times New Roman";

    private TrayIcon trayIcon;
    private Font regularFont, captiveFont;

    private Window window;
    private PopupMenu menu;
    private Configuration config;
    private ResourceBundle resourceBundle;
    private MenuItem exitMenuItem, openMenuItem, settingsMenuItem;

    public Tray(Configuration config, Window window) throws SystemTrayNotSupportedException {
        this.config = config;
        this.window = window;

        if (SystemTray.isSupported()) {
            try {
                resourceBundle = LocaleResource.getLocaleResourceBundle();

                trayIcon = new TrayIcon(VECServer.ICON, resourceBundle.getString("SERVER_APP_TITLE"));
                trayIcon.setImageAutoSize(true);
                trayIcon.addMouseListener(this);

                captiveFont = new Font(FONT, Font.BOLD, FONT_SIZE);
                regularFont = new Font(FONT, Font.PLAIN, FONT_SIZE);

                menu = new PopupMenu();
                trayIcon.setPopupMenu(menu);

                openMenuItem = new MenuItem(resourceBundle.getString("SERVER_TRAY_OPEN"));
                openMenuItem.setFont(captiveFont);
                openMenuItem.addActionListener(this);
                menu.add(openMenuItem);

                settingsMenuItem = new MenuItem(resourceBundle.getString("SERVER_TRAY_SETTINGS"));
                settingsMenuItem.setFont(regularFont);
                settingsMenuItem.addActionListener(this);
                menu.add(settingsMenuItem);

                menu.addSeparator();

                exitMenuItem = new MenuItem(resourceBundle.getString("SERVER_TRAY_EXIT"));
                exitMenuItem.setFont(regularFont);
                exitMenuItem.addActionListener(this);
                menu.add(exitMenuItem);

                SystemTray tray = SystemTray.getSystemTray();
                tray.add(trayIcon);
            } catch (AWTException e) {
                throw new SystemTrayNotSupportedException();
            }
        }
    }

    public MenuItem getOpenMenuItem() {
        return openMenuItem;
    }

    public MenuItem getSettingsMenuItem() {
        return settingsMenuItem;
    }

    public MenuItem getExitMenuItem() {
        return exitMenuItem;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object o = e.getSource();

        if (o.equals(openMenuItem)) {
            if (window != null) {
                window.setVisible(true);
                window.setState(JFrame.NORMAL);
            }
        } else if (o.equals(settingsMenuItem)) {
            new SettingsDialog(config, window);
        } else if (o.equals(exitMenuItem)) {
            System.exit(0);
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        trayIcon.displayMessage("", resourceBundle.getString("SERVER_APP_TITLE"), TrayIcon.MessageType.INFO);
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }
}
