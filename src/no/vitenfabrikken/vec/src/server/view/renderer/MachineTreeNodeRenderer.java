package no.vitenfabrikken.vec.src.server.view.renderer;

import no.vitenfabrikken.vec.src.library.utils.locale.LocaleResource;
import no.vitenfabrikken.vec.src.server.model.config.Configuration;
import no.vitenfabrikken.vec.src.server.model.machine.Computer;
import no.vitenfabrikken.vec.src.server.model.machine.Machine;
import no.vitenfabrikken.vec.src.server.model.machine.Projector;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import java.awt.*;
import java.util.ResourceBundle;

public class MachineTreeNodeRenderer extends DefaultTreeCellRenderer {
    private static final ImageIcon ON_IMAGE_PATH = new ImageIcon("images/status/on.png");
    private static final ImageIcon OFF_IMAGE_PATH = new ImageIcon("images/status/off.png");
    private static final ImageIcon ERROR_IMAGE_PATH = new ImageIcon("images/status/error.png");

    private ResourceBundle resourceBundle;

    public MachineTreeNodeRenderer() {
        resourceBundle = LocaleResource.getLocaleResourceBundle();
    }

    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded,
                                                  boolean leaf, int row, boolean hasFocus) {
        super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);

        if (leaf) {
            Object o = ((DefaultMutableTreeNode) value).getUserObject();
            if (o instanceof Machine) {
                Configuration config = Configuration.getCurrentConfiguration();
                Machine machine = (Machine) o;
                setText(machine.getName() + (machine.getStatus() != null &&
                        machine.getStatus().equals(config.getAlwaysOnlineText()) &&
                        !config.getAlwaysOnlineText().isEmpty() ? " (" + config.getAlwaysOnlineText() + ")" : ""));

                if (machine.getError() != null && machine.getStatus() != null) {
                    if (machine instanceof Computer) {
                        Computer computer = (Computer) machine;
                        if ((computer.getStatus().equals(resourceBundle.getString("STATUS_STARTING_UP")) ||
                                computer.getStatus().equals(resourceBundle.getString("STATUS_RESTARTING"))) &&
                                computer.getError().equals(resourceBundle.getString("ERROR_UNABLE_TO_START"))) {
                            setIcon(ERROR_IMAGE_PATH);
                        } else if (!computer.getError().equals(resourceBundle.getString("ERROR_NO_ERROR")) &&
                                !computer.getError().equals(resourceBundle.getString("ERROR_UNABLE_TO_START"))) {
                            setIcon(ERROR_IMAGE_PATH);
                        } else if (computer.getStatus().equals(resourceBundle.getString("STATUS_ONLINE")) ||
                                computer.getStatus().equals(resourceBundle.getString("STATUS_STARTING_UP")) ||
                                computer.getStatus().equals(config.getAlwaysOnlineText())) {
                            setIcon(ON_IMAGE_PATH);
                        } else {
                            setIcon(OFF_IMAGE_PATH);
                        }
                    } else if (machine instanceof Projector) {
                        Projector projector = (Projector) machine;
                        if (projector.getError() != null && (
                                (!projector.getError().equals(resourceBundle.getString("PROJECTOR_NO_ERROR")) &&
                                        !projector.getError().equals(resourceBundle.getString("ERROR_NOT_AVAILABLE"))) ||
                                        projector.getStatus().equals(resourceBundle.getString("PROJECTOR_NO_CONNECTION")))) {
                            setIcon(ERROR_IMAGE_PATH);
                        } else if (projector.getStatus().startsWith(resourceBundle.getString("PROJECTOR_ON")) ||
                                projector.getStatus().equals(resourceBundle.getString("PROJECTOR_POWERING_ON"))) {
                            setIcon(ON_IMAGE_PATH);
                        } else {
                            setIcon(OFF_IMAGE_PATH);
                        }
                    }
                } else {
                    setIcon(OFF_IMAGE_PATH);
                }
            }
        }

        return this;
    }
}