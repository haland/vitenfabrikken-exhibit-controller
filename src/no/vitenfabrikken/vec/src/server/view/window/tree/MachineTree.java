package no.vitenfabrikken.vec.src.server.view.window.tree;

import no.vitenfabrikken.vec.src.server.model.list.MachineList;
import no.vitenfabrikken.vec.src.server.model.list.listeners.MachineListChangedEvent;
import no.vitenfabrikken.vec.src.server.model.list.listeners.MachineListChangedListener;
import no.vitenfabrikken.vec.src.server.model.machine.Machine;
import no.vitenfabrikken.vec.src.server.model.machine.listeners.MachineSettingsChangedEvent;
import no.vitenfabrikken.vec.src.server.model.machine.listeners.MachineSettingsChangedListener;
import no.vitenfabrikken.vec.src.server.model.tree.MachineTreeNode;
import no.vitenfabrikken.vec.src.server.view.renderer.MachineTreeNodeRenderer;
import no.vitenfabrikken.vec.src.server.view.window.popup.MachinePopupMenu;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MachineTree extends JPanel implements TreeSelectionListener, MachineListChangedListener,
        MachineSettingsChangedListener {
    private static final long serialVersionUID = 4296481325212019244L;

    private JTree tree;
    private JScrollPane scroll;
    private MachineList machineList;
    private Machine[] selectedMachines;
    private DefaultMutableTreeNode head;
    private MachineTreeNodeRenderer machineTreeNodeRenderer;

    public MachineTree(MachineList machineList) {
        this.machineList = machineList;
        this.machineList.addMachineListChangedListener(this);
        this.machineTreeNodeRenderer = new MachineTreeNodeRenderer();

        repaintTree();
    }

    public void valueChanged(TreeSelectionEvent e) {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode)
                tree.getLastSelectedPathComponent();

        if (node == null) {
            selectedMachines = null;
            return;
        }

        Machine m = null;
        if (node.getUserObject() instanceof Machine)
            m = (Machine) node.getUserObject();
        selectedMachines = new Machine[1];
        selectedMachines[0] = m;
    }

    @Override
    public void settingsChange(MachineSettingsChangedEvent e) {
        repaintTree();
    }

    @Override
    public void machineListChanged(MachineListChangedEvent e) {
        if (e.getType() == MachineListChangedEvent.REMOVED)
            e.getSource().removeSettingsChangedListener(this);

        repaintTree();
    }

    private void repaintTree() {
        removeAll();

        head = new DefaultMutableTreeNode("VEC Group");

        for (Machine machine : machineList) {
            machine.addSettingsChangedListener(this);
            MachineTreeNode node = new MachineTreeNode(machine);
            head.add(node);
        }

        tree = new JTree(head);
        tree.setRootVisible(false);
        tree.setCellRenderer(machineTreeNodeRenderer);
        tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        tree.addTreeSelectionListener(this);
        tree.addMouseListener(new TreeMouseEvent(tree));
        tree.setVisibleRowCount(20);

        scroll = new JScrollPane(tree);
        scroll.setPreferredSize(new Dimension(340, 320));
        add(scroll);

        validate();
    }

    private class TreeMouseEvent extends MouseAdapter {
        private JTree tree;

        public TreeMouseEvent(JTree tree) {
            this.tree = tree;
        }

        public void popupEvent(MouseEvent e) {
            int x = e.getX();
            int y = e.getY();

            TreePath path = tree.getPathForLocation(x, y);

            if (path == null)
                return;

            tree.setSelectionPath(path);

            DefaultMutableTreeNode node = (DefaultMutableTreeNode)
                    path.getLastPathComponent();
            Object o = node.getUserObject();

            if (o instanceof Machine) {
                Machine m = (Machine) o;
                MachinePopupMenu popup = new MachinePopupMenu(m, machineList);
                popup.show(tree, x, y);
            }
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            if (e.getButton() == MouseEvent.BUTTON3)
                popupEvent(e);
        }
    }
}
