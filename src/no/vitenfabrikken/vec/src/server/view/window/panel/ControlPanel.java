package no.vitenfabrikken.vec.src.server.view.window.panel;

import no.vitenfabrikken.vec.src.library.utils.locale.LocaleResource;
import no.vitenfabrikken.vec.src.server.controller.machine.actions.MachineShutdownListener;
import no.vitenfabrikken.vec.src.server.controller.machine.actions.MachineWakeListener;
import no.vitenfabrikken.vec.src.server.model.list.MachineList;
import no.vitenfabrikken.vec.src.server.view.window.Window;
import no.vitenfabrikken.vec.src.server.view.window.button.MachineButton;

import javax.swing.*;
import java.awt.*;
import java.util.ResourceBundle;


public class ControlPanel extends JPanel {
    private static final long serialVersionUID = -929177240121198182L;

    private MachineButton allOn, allOff;
    private ResourceBundle resourceBundle;

    public ControlPanel(MachineList list) {
        super(new GridLayout(1, 2));
        resourceBundle = LocaleResource.getLocaleResourceBundle();

        allOn = new MachineButton(resourceBundle.getString("UNIVERSAL_TURN_ALL_ON"), list);
        allOn.setSize(Window.WIDTH / 2 - 10, 30);
        allOn.addMachineActionButtonListener(new MachineWakeListener());

        allOff = new MachineButton(resourceBundle.getString("UNIVERSAL_TURN_ALL_OFF"), list);
        allOff.setSize(Window.WIDTH / 2 - 10, 30);
        allOff.addMachineActionButtonListener(new MachineShutdownListener());

        add(allOn);
        add(allOff);
    }
}
