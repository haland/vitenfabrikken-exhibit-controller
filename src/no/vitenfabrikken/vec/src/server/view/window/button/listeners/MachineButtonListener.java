package no.vitenfabrikken.vec.src.server.view.window.button.listeners;

import no.vitenfabrikken.vec.src.server.model.machine.Machine;

public interface MachineButtonListener {
    public void buttonPressed(Machine[] machines);
}
