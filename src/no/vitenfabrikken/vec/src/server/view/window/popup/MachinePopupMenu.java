package no.vitenfabrikken.vec.src.server.view.window.popup;

import no.vitenfabrikken.vec.src.library.utils.locale.LocaleResource;
import no.vitenfabrikken.vec.src.server.controller.log.ServerLogger;
import no.vitenfabrikken.vec.src.server.controller.machine.actions.MachineRestartListener;
import no.vitenfabrikken.vec.src.server.controller.machine.actions.MachineShutdownListener;
import no.vitenfabrikken.vec.src.server.controller.machine.actions.MachineWakeListener;
import no.vitenfabrikken.vec.src.server.model.list.MachineList;
import no.vitenfabrikken.vec.src.server.model.machine.Computer;
import no.vitenfabrikken.vec.src.server.model.machine.Machine;
import no.vitenfabrikken.vec.src.server.view.dialog.machine.delete.DeleteDialog;
import no.vitenfabrikken.vec.src.server.view.dialog.machine.properties.PropertiesDialog;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ResourceBundle;
import java.util.logging.Level;


public class MachinePopupMenu extends JPopupMenu {
    private static final long serialVersionUID = 9181069203482598094L;

    private static final String VNC_VIEWER = "vnc" + File.separator + "tvnviewer.exe";

    private MachineList machineList;
    private Machine[] machine;
    private ResourceBundle resourceBundle;
    private JMenuItem wake, restart, shutdown, remove, remote, properties;

    public MachinePopupMenu(Machine m, MachineList list) {
        machineList = list;
        resourceBundle = LocaleResource.getLocaleResourceBundle();

        machine = new Machine[1];
        machine[0] = m;

        wake = new JMenuItem(resourceBundle.getString("MACHINE_POPUP_MENU_TURN_ON"));
        wake.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MachineWakeListener wakeListener = new MachineWakeListener();
                wakeListener.buttonPressed(machine);
            }
        });
        add(wake);

        shutdown = new JMenuItem(resourceBundle.getString("MACHINE_POPUP_MENU_TURN_OFF"));
        shutdown.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MachineShutdownListener shutdownListener = new MachineShutdownListener();
                shutdownListener.buttonPressed(machine);
            }
        });
        add(shutdown);

        if (m instanceof Computer) {
            restart = new JMenuItem(resourceBundle.getString("MACHINE_POPUP_MENU_RESTART"));
            restart.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    MachineRestartListener restartListener = new MachineRestartListener();
                    restartListener.buttonPressed(machine);
                }
            });
            add(restart);
        }

        addSeparator();

        remove = new JMenuItem(resourceBundle.getString("MACHINE_POPUP_MENU_REMOVE"));
        remove.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new DeleteDialog(machine[0], machineList);
            }
        });
        add(remove);

        addSeparator();

        if (m instanceof Computer) {
            remote = new JMenuItem(resourceBundle.getString("MACHINE_POPUP_MENU_REMOTE"));
            remote.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent arg) {
                    try {
                        Runtime.getRuntime().exec(VNC_VIEWER + " " + machine[0].getIp());
                    } catch (IOException e) {
                        ServerLogger.logStackTrace(Level.WARNING, e.getStackTrace());
                        e.printStackTrace();
                    }
                }
            });
            add(remote);
        }

        properties = new JMenuItem(resourceBundle.getString("MACHINE_POPUP_MENU_PROPERTIES"));
        properties.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg) {
                try {
                    new PropertiesDialog(machine[0]);
                } catch (Exception e) {
                    ServerLogger.logStackTrace(Level.WARNING, e.getStackTrace());
                    e.printStackTrace();
                }
            }
        });
        add(properties);
    }
}
