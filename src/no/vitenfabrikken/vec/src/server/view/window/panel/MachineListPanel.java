package no.vitenfabrikken.vec.src.server.view.window.panel;

import no.vitenfabrikken.vec.src.library.view.layout.SpringUtilities;
import no.vitenfabrikken.vec.src.server.model.list.MachineList;
import no.vitenfabrikken.vec.src.server.view.window.tree.MachineTree;

import javax.swing.*;
import java.awt.*;


public class MachineListPanel extends JPanel {
    private static final long serialVersionUID = 1191318763908265679L;

    private MachineTree objectTree;

    public MachineListPanel(MachineList machineList) {
        super(new SpringLayout());

        objectTree = new MachineTree(machineList);
        add(objectTree);

        setMinimumSize(new Dimension(260, 400));
        SpringUtilities.makeCompactGrid(this, getComponentCount(), 1, 5, 5, 5, 5);
    }
}
