package no.vitenfabrikken.vec.src.server.view.window.button;

import no.vitenfabrikken.vec.src.server.model.list.MachineList;
import no.vitenfabrikken.vec.src.server.model.machine.Machine;
import no.vitenfabrikken.vec.src.server.view.window.button.listeners.MachineButtonListener;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.Iterator;
import java.util.Vector;


public class MachineButton extends JButton {
    private static final long serialVersionUID = 7926056524692144571L;

    private MachineList list;
    private Vector<MachineButtonListener> listeners;

    public MachineButton(String name, MachineList list) {
        super(name);
        this.list = list;
        this.listeners = new Vector<MachineButtonListener>();
    }

    @Override
    protected void fireActionPerformed(ActionEvent event) {
        Iterator<MachineButtonListener> i = listeners.iterator();
        while (i.hasNext())
            i.next().buttonPressed(list.toArray(new Machine[list.size()]));
    }

    public void addMachineActionButtonListener(MachineButtonListener l) {
        if (!listeners.contains(l))
            listeners.add(l);
    }

    public void removeMachineActionButtonListener(MachineButtonListener l) {
        if (listeners.contains(l))
            listeners.remove(l);
    }
}
