package no.vitenfabrikken.vec.src.server.view.window;

import no.vitenfabrikken.vec.src.library.utils.locale.LocaleResource;
import no.vitenfabrikken.vec.src.server.VECServer;
import no.vitenfabrikken.vec.src.server.model.config.Configuration;
import no.vitenfabrikken.vec.src.server.model.list.MachineList;
import no.vitenfabrikken.vec.src.server.view.menu.MenuBar;
import no.vitenfabrikken.vec.src.server.view.window.panel.ControlPanel;
import no.vitenfabrikken.vec.src.server.view.window.panel.MachineListPanel;

import javax.swing.*;
import java.awt.*;
import java.util.ResourceBundle;

public class Window extends JFrame {
    private static final long serialVersionUID = -2839559430391900797L;

    private MenuBar menuBar;
    private JPanel container;

    public Window(Configuration config, MachineList machineList) {
        super();
        ResourceBundle resourceBundle = LocaleResource.getLocaleResourceBundle();

        menuBar = new MenuBar(config, machineList, this);
        container = new JPanel(new BorderLayout());

        container.add(new MachineListPanel(machineList), BorderLayout.CENTER);
        container.add(new ControlPanel(machineList), BorderLayout.SOUTH);

        add(container);

        setJMenuBar(menuBar);
        setIconImage(VECServer.ICON);

        pack();

        setPosition();
        setVisible(true);
        setResizable(true);
        setMinimumSize(new Dimension(260, 424));
        setPreferredSize(new Dimension(260, 424));
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        setTitle(resourceBundle.getString("SERVER_APP_TITLE"));
    }

    private void setPosition() {
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();

        int xPos = screen.width / 2 - getWidth() / 2;
        int yPos = screen.height / 2 - getHeight() / 2;

        setLocation(xPos, yPos);
    }
}