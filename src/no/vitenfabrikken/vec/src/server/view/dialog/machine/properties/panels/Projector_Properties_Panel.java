package no.vitenfabrikken.vec.src.server.view.dialog.machine.properties.panels;

import no.vitenfabrikken.vec.src.library.utils.locale.LocaleResource;
import no.vitenfabrikken.vec.src.library.view.layout.SpringUtilities;
import no.vitenfabrikken.vec.src.server.model.machine.Projector;

import javax.swing.*;
import java.util.ResourceBundle;

public class Projector_Properties_Panel extends PropertiesPanel {
    private static final long serialVersionUID = -3421804388337321462L;

    public Projector_Properties_Panel(Projector p) {
        super(new SpringLayout());
        ResourceBundle resourceBundle = LocaleResource.getLocaleResourceBundle();

        add(addProperty(resourceBundle.getString("PROPERTIES_DIALOG_IP"), p.getIp()));

        add(addProperty(resourceBundle.getString("PROPERTIES_DIALOG_PORT"), String.valueOf(p.getPort())));

        add(addProperty(resourceBundle.getString("PROPERTIES_DIALOG_STATUS"), (p.getStatus() == null ||
                p.getStatus().equals("")) ? resourceBundle.getString("STATUS_NOT_AVAILABLE") : getStatus(p)));

        add(addProperty(resourceBundle.getString("PROPERTIES_DIALOG_ERROR"), (p.getError() == null ||
                p.getError().equals("")) ? resourceBundle.getString("ERROR_NOT_AVAILABLE") : getError(p)));

        add(addProperty(resourceBundle.getString("PROPERTIES_DIALOG_FILTER_HOURS_TITLE"),
                p.getFilterHours() == Projector.FILTER_HOURS_NOT_AVAILABLE ?
                        resourceBundle.getString("STATUS_NOT_AVAILABLE") : String.valueOf(p.getFilterHours())));

        add(addProperty(resourceBundle.getString("PROPERTIES_DIALOG_LAMP_HOURS_TITLE"),
                p.getLampHours() == Projector.LAMP_HOURS_NOT_AVAILABLE ?
                        resourceBundle.getString("STATUS_NOT_AVAILABLE") : String.valueOf(p.getLampHours())));

        SpringUtilities.makeGrid(this, getComponentCount(), 1, 5, 5, 5, 5);
    }
}
