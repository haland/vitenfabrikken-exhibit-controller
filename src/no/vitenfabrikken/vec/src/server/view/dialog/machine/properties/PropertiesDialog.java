package no.vitenfabrikken.vec.src.server.view.dialog.machine.properties;

import no.vitenfabrikken.vec.src.library.utils.locale.LocaleResource;
import no.vitenfabrikken.vec.src.library.view.dialog.VECDialog;
import no.vitenfabrikken.vec.src.server.VECServer;
import no.vitenfabrikken.vec.src.server.model.machine.Computer;
import no.vitenfabrikken.vec.src.server.model.machine.Machine;
import no.vitenfabrikken.vec.src.server.model.machine.Projector;
import no.vitenfabrikken.vec.src.server.view.dialog.machine.properties.panels.Computer_Properties_Panel;
import no.vitenfabrikken.vec.src.server.view.dialog.machine.properties.panels.Projector_Properties_Panel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ResourceBundle;


public class PropertiesDialog extends VECDialog implements WindowListener, ActionListener {
    private static final long serialVersionUID = 4264194308742988962L;

    public PropertiesDialog(Machine m) {
        super(VECServer.getInstance().getWindow(), LocaleResource.getLocaleResourceBundle()
                .getString("PROPERTIES_DIALOG_TITLE") + " \"" + m.getName() + "\" - " +
                LocaleResource.getLocaleResourceBundle().getString("SERVER_APP_TITLE"), true);
        setLayout(new BorderLayout());

        ResourceBundle resourceBundle = LocaleResource.getLocaleResourceBundle();

        JPanel container = null;
        if (m instanceof Computer) {
            container = new Computer_Properties_Panel((Computer) m);
        } else if (m instanceof Projector) {
            container = new Projector_Properties_Panel((Projector) m);
        }

        if (container != null) {
            JButton button = new JButton(resourceBundle.getString("PROPERTIES_DIALOG_OK_BUTTON"));
            button.addActionListener(this);

            add(container, BorderLayout.CENTER);
            add(button, BorderLayout.SOUTH);

            pack();
            setPosition();
            addWindowListener(this);
            setIconImage(VECServer.ICON);
            setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

            setVisible(true);
        } else dispose();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        windowClosing(new WindowEvent(this, 1));
    }

    @Override
    public void windowClosing(WindowEvent e) {
        dispose();
    }

    public void windowOpened(WindowEvent e) {
    }

    public void windowClosed(WindowEvent e) {
    }

    public void windowActivated(WindowEvent e) {
    }

    public void windowIconified(WindowEvent e) {
    }

    public void windowDeactivated(WindowEvent e) {
    }

    public void windowDeiconified(WindowEvent e) {
    }
}
