package no.vitenfabrikken.vec.src.server.view.dialog.settings;

import no.vitenfabrikken.vec.src.library.utils.locale.LocaleResource;
import no.vitenfabrikken.vec.src.library.utils.network.TCPSocket;
import no.vitenfabrikken.vec.src.library.view.dialog.VECDialog;
import no.vitenfabrikken.vec.src.library.view.layout.SpringUtilities;
import no.vitenfabrikken.vec.src.server.VECServer;
import no.vitenfabrikken.vec.src.server.controller.log.ServerLogger;
import no.vitenfabrikken.vec.src.server.model.config.Configuration;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ResourceBundle;
import java.util.logging.Level;

public class SettingsDialog extends VECDialog implements WindowListener, ActionListener {
    private static final long serialVersionUID = 256539971299513449L;

    private JPanel panel;
    private JTextField portTextField, alwaysOnlineTextTextField;
    private JLabel portDescriptionLabel, alwaysOnlineTextLabel;
    private JButton saveButton, cancelButton;

    private Configuration config;
    private ResourceBundle resourceBundle;

    public SettingsDialog(Configuration config, JFrame frame) {
        super(frame, LocaleResource.getLocaleResourceBundle().getString("SERVER_SETTINGS_TITLE"), true);
        setLayout(new BorderLayout());

        this.config = config;
        this.resourceBundle = LocaleResource.getLocaleResourceBundle();

        panel = new JPanel(new SpringLayout());

        portDescriptionLabel = new JLabel(resourceBundle.getString("SERVER_SETTINGS_PORT"));
        panel.add(portDescriptionLabel);

        String currentPortText = "";
        if (config != null) {
            currentPortText = String.valueOf(config.getPort());
        }

        portTextField = new JTextField(currentPortText);
        panel.add(portTextField);

        alwaysOnlineTextLabel = new JLabel(resourceBundle.getString("SERVER_SETTINGS_ALWAYS_ONLINE"));
        panel.add(alwaysOnlineTextLabel);

        String currentAlwaysOnlineText = resourceBundle.getString("STATUS_ALWAYS_ON");
        if (config != null && config.getAlwaysOnlineText() != null) {
            currentAlwaysOnlineText = config.getAlwaysOnlineText();
        }

        alwaysOnlineTextTextField = new JTextField(currentAlwaysOnlineText);
        panel.add(alwaysOnlineTextTextField);

        saveButton = new JButton(resourceBundle.getString("SERVER_SETTINGS_SAVE"));
        saveButton.addActionListener(this);
        panel.add(saveButton);

        cancelButton = new JButton(resourceBundle.getString("SERVER_SETTINGS_CANCEL"));
        cancelButton.addActionListener(this);
        panel.add(cancelButton);

        SpringUtilities.makeCompactGrid(panel, panel.getComponentCount(), 1, 5, 5, 5, 5);
        add(panel, BorderLayout.CENTER);

        pack();
        setPosition();
        setIconImage(VECServer.ICON);
        setResizable(false);
        addWindowListener(this);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object o = e.getSource();

        if (o.equals(saveButton)) {
            try {
                if (config == null) {
                    Configuration config = new Configuration(Integer.parseInt(portTextField.getText()),
                            alwaysOnlineTextTextField.getText());
                    config.save();
                    ServerLogger.logMessage(Level.INFO, "Server configuration created:\n" +
                            "   Port: " + portTextField.getText() + "\n" + "    AlwaysOnlineText: " +
                            alwaysOnlineTextTextField.getText());
                    System.out.println("Server configuration created:\n" +
                            "   Port: " + portTextField.getText() + "\n" + "    AlwaysOnlineText: " +
                            alwaysOnlineTextTextField.getText());
                } else {
                    int port = Integer.parseInt(portTextField.getText());

                    boolean portChanged = port != config.getPort();
                    boolean alwaysOnlineTextChanged = !alwaysOnlineTextTextField.getText()
                            .equals(config.getAlwaysOnlineText());

                    if (portChanged || alwaysOnlineTextChanged) {
                        if (port == config.getPort() || TCPSocket.available(port)) {
                            config.setPort(port);
                            config.setAlwaysOnlineText(alwaysOnlineTextTextField.getText());
                        } else {
                            portTextField.setText(resourceBundle.getString("SERVER_SETTINGS_ILLEGAL_PORT"));
                        }

                        String logMessage = "The server configuration changed:\n" +
                                "   Port: " + config.getPort() + "\n" + "   AlwaysOnlineText: " +
                                config.getAlwaysOnlineText();
                        ServerLogger.logMessage(Level.WARNING, logMessage);
                        System.out.println(logMessage);
                    }
                }

                dispose();
            } catch (IllegalArgumentException arg) {
                portTextField.setText(resourceBundle.getString("SERVER_SETTINGS_ILLEGAL_PORT"));
            }

        } else if (o.equals(cancelButton)) {
            if (Configuration.configExists())
                VECServer.getInstance();
            dispose();
        }
    }

    @Override
    public void windowClosing(WindowEvent e) {
        dispose();
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }
}
