package no.vitenfabrikken.vec.src.server.view.dialog.progress;

import no.vitenfabrikken.vec.src.library.view.dialog.VECDialog;
import no.vitenfabrikken.vec.src.library.view.layout.SpringUtilities;
import no.vitenfabrikken.vec.src.server.VECServer;

import javax.swing.*;
import java.awt.*;


public class ProgressBarDialog extends VECDialog {
    private static final long serialVersionUID = -2409373655842692672L;

    private int maxValue;
    private JPanel panel;
    private JLabel progressLabel;
    private JProgressBar progressBar;

    public ProgressBarDialog(String title, int maxValue) {
        super(VECServer.getInstance().getWindow(), title, false);
        this.maxValue = maxValue;

        setLayout(new BorderLayout());
        setIconImage(VECServer.ICON);

        progressLabel = new JLabel("");
        progressBar = new JProgressBar(0, maxValue);

        panel = new JPanel(new SpringLayout());
        panel.add(progressLabel);
        panel.add(progressBar);

        SpringUtilities.makeCompactGrid(panel, 2, 1, 5, 5, 5, 5);
        add(panel, BorderLayout.CENTER);

        setSize(400, 100);
        setPosition();

        setVisible(true);
        setResizable(false);
        setAlwaysOnTop(true);
    }

    public void setProgress(String name) {
        progressLabel.setText(name);
    }

    public void setPercent(int percent) {
        progressBar.setValue(percent);

        if (progressBar.getValue() >= maxValue)
            dispose();
    }
}
