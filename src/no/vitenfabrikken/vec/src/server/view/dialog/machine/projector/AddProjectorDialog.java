package no.vitenfabrikken.vec.src.server.view.dialog.machine.projector;

import no.vitenfabrikken.vec.src.library.utils.file.ObjectXMLWriter;
import no.vitenfabrikken.vec.src.library.utils.file.entries.PackageEntries;
import no.vitenfabrikken.vec.src.library.utils.locale.LocaleResource;
import no.vitenfabrikken.vec.src.library.view.dialog.VECDialog;
import no.vitenfabrikken.vec.src.library.view.layout.SpringUtilities;
import no.vitenfabrikken.vec.src.server.VECServer;
import no.vitenfabrikken.vec.src.server.controller.log.ServerLogger;
import no.vitenfabrikken.vec.src.server.controller.machine.MachineHandler;
import no.vitenfabrikken.vec.src.server.model.list.MachineList;
import no.vitenfabrikken.vec.src.server.model.machine.Machine;
import no.vitenfabrikken.vec.src.server.model.machine.Projector;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;

public class AddProjectorDialog extends VECDialog implements WindowListener, ActionListener {
    private static final long serialVersionUID = -6554368024735885498L;

    private static final int SUPPORTED_PROJECTOR_NAMES = 0;
    private static final int SUPPORTED_PROJECTOR_CLASSES = 1;
    private static final String MACHINE_TYPE_PROJECTOR = "no.vitenfabrikken.vec.src.server.model.machine.projectors.";

    @SuppressWarnings("rawtypes")
    private JComboBox typeList;
    private String[][] supportedProjectors;
    private int selectedIndex;

    private JPanel panel;
    private JButton save, cancel;
    private MachineList machineList;
    private ResourceBundle resourceBundle;
    private JTextField nameText, ipText, portText;
    private JLabel nameDescription, ipDescription, portDescription, typesLabel;

    public AddProjectorDialog(MachineList machineList) {
        new AddProjectorDialog(machineList, 0);
    }

    public AddProjectorDialog(MachineList machineList, int selectedIndex) {
        new AddProjectorDialog(machineList, selectedIndex, null, null);
    }

    public AddProjectorDialog(MachineList machineList, int selectedIndex, String name, String ip) {
        super(VECServer.getInstance().getWindow(),
                LocaleResource.getLocaleResourceBundle().getString("ADD_PROJECTOR_TITLE"), true);
        this.selectedIndex = selectedIndex;
        this.machineList = machineList;
        this.supportedProjectors = getSupportedProjectors();
        this.resourceBundle = LocaleResource.getLocaleResourceBundle();

        panel = new JPanel(new SpringLayout());

        addName(panel, name);
        addIP(panel, ip);
        addPort(panel);
        addTypeList(panel, selectedIndex);

        save = new JButton(resourceBundle.getString("ADD_PROJECTOR_SAVE_BUTTON"));
        save.addActionListener(this);
        panel.add(save);

        cancel = new JButton(resourceBundle.getString("ADD_PROJECTOR_CANCEL_BUTTON"));
        cancel.addActionListener(this);
        panel.add(cancel);

        SpringUtilities.makeCompactGrid(panel, panel.getComponentCount(), 1, 5, 5, 5, 5);
        add(panel);

        setIconImage(VECServer.ICON);

        pack();
        setPosition();
        setVisible(true);
        setAlwaysOnTop(true);
        addWindowListener(this);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    }

    private static String[][] getSupportedProjectors() {
        String[][] supportedProjectors = new String[2][];
        ArrayList<String> names = new ArrayList<String>();
        ArrayList<String> classes = new ArrayList<String>();

        ArrayList<String> list = PackageEntries.getClassNamesFromPackage(MACHINE_TYPE_PROJECTOR);
        for (String name : list) {
            names.add(name.replace("_", " "));
            classes.add(MACHINE_TYPE_PROJECTOR + name);
        }

        supportedProjectors[SUPPORTED_PROJECTOR_NAMES] = names.toArray(new String[names.size()]);
        supportedProjectors[SUPPORTED_PROJECTOR_CLASSES] = classes.toArray(new String[classes.size()]);

        return supportedProjectors;
    }

    private void addName(JPanel panel, String name) {
        nameDescription = new JLabel(resourceBundle.getString("ADD_PROJECTOR_NAME_TEXT"));
        nameText = new JTextField(name != null ? name : "");
        panel.add(nameDescription);
        panel.add(nameText);
    }

    private void addIP(JPanel panel, String ip) {
        ipDescription = new JLabel(resourceBundle.getString("ADD_PROJECTOR_IP_TEXT"));
        ipText = new JTextField(ip != null ? ip : "");
        panel.add(ipDescription);
        panel.add(ipText);
    }

    private void addPort(JPanel panel) {
        String defaultPort = "";
        try {
            Projector p = (Projector) Class.forName(supportedProjectors[SUPPORTED_PROJECTOR_CLASSES][selectedIndex]).
                    getConstructor().newInstance();
            defaultPort = String.valueOf(p.getDefaultPort());
        } catch (Exception e) {
            ServerLogger.logStackTrace(Level.SEVERE, e.getStackTrace());
        }

        portDescription = new JLabel(resourceBundle.getString("ADD_PROJECTOR_PORT_TEXT"));
        portText = new JTextField(defaultPort);
        panel.add(portDescription);
        panel.add(portText);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private void addTypeList(JPanel panel, int selectedIndex) {
        typesLabel = new JLabel(resourceBundle.getString("ADD_PROJECTOR_TYPE_TEXT"));

        typeList = new JComboBox(supportedProjectors[SUPPORTED_PROJECTOR_NAMES]);
        typeList.addActionListener(this);
        typeList.setSelectedIndex(selectedIndex);

        panel.add(typesLabel);
        panel.add(typeList);
    }

    private void save() {
        String[] projectorTypes = (String[]) supportedProjectors[SUPPORTED_PROJECTOR_CLASSES];

        try {
            Object o = Class.forName(projectorTypes[selectedIndex]).newInstance();
            Projector p = (Projector) o;

            p.setIp(ipText.getText());
            p.setName(nameText.getText());
            p.setPort(Integer.parseInt(portText.getText()));
            p.setLampHours(Projector.LAMP_HOURS_NOT_AVAILABLE);
            p.setFilterHours(Projector.FILTER_HOURS_NOT_AVAILABLE);
            p.setStatus(resourceBundle.getString("PROJECTOR_OFF"));
            p.setError(resourceBundle.getString("PROJECTOR_NO_ERROR"));

            File f = new File(MachineHandler.getFilePath(p.getName()));
            ObjectXMLWriter.write(Machine.class, p, f);

            machineList.add(p);
            ServerLogger.logMessage(Level.INFO, "A new projector has been added to the system:'" + p.getName() + "'.");
            System.out.println("A new projector has been added to the system:'" + p.getName() + "'.");
        } catch (Exception e) {
            ServerLogger.logStackTrace(Level.WARNING, e.getStackTrace());
            e.printStackTrace();
        }

        windowClosing(new WindowEvent(this, 1));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(save)) {
            save();
        } else if (e.getSource().equals(cancel)) {
            windowClosing(new WindowEvent(this, 1));
        } else if (e.getSource().equals(typeList)) {
            if (selectedIndex != typeList.getSelectedIndex()) {
                windowClosing(new WindowEvent(this, 1));
                new AddProjectorDialog(machineList, typeList.getSelectedIndex(), nameText.getText(),
                        ipText.getText());
            }
        }
    }

    @Override
    public void windowClosing(WindowEvent e) {
        dispose();
    }

    public void windowOpened(WindowEvent e) {
    }

    public void windowClosed(WindowEvent e) {
    }

    public void windowActivated(WindowEvent e) {
    }

    public void windowIconified(WindowEvent e) {
    }

    public void windowDeactivated(WindowEvent e) {
    }

    public void windowDeiconified(WindowEvent e) {
    }
}
