package no.vitenfabrikken.vec.src.server.view.dialog.machine.properties.panels;

import no.vitenfabrikken.vec.src.library.view.layout.SpringUtilities;
import no.vitenfabrikken.vec.src.server.model.machine.Machine;

import javax.swing.*;
import java.awt.*;

public class PropertiesPanel extends JPanel {
    private static final long serialVersionUID = -8870539929992328554L;

    PropertiesPanel(LayoutManager layoutManager) {
        super(layoutManager);
    }

    String getError(Machine m) {
        char[] characters = m.getError().toCharArray();
        characters[0] = Character.toUpperCase(characters[0]);

        return new String(characters);
    }

    String getStatus(Machine m) {
        char[] characters = m.getStatus().toCharArray();
        characters[0] = Character.toUpperCase(characters[0]);

        return new String(characters);
    }

    JPanel addProperty(String title, String value) {
        JTextField text = new JTextField(value);
        text.setEditable(false);

        JPanel panel = new JPanel(new SpringLayout());
        panel.add(new JLabel(title));
        panel.add(text);

        SpringUtilities.makeGrid(panel, panel.getComponentCount(), 1, 5, 5, 5, 5);
        return panel;
    }
}
