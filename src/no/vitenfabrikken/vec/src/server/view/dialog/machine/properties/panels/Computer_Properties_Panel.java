package no.vitenfabrikken.vec.src.server.view.dialog.machine.properties.panels;

import no.vitenfabrikken.vec.src.library.utils.locale.LocaleResource;
import no.vitenfabrikken.vec.src.library.view.layout.SpringUtilities;
import no.vitenfabrikken.vec.src.server.model.machine.Computer;

import javax.swing.*;
import java.util.ResourceBundle;

public class Computer_Properties_Panel extends PropertiesPanel {
    private static final long serialVersionUID = 5673750946492860166L;

    public Computer_Properties_Panel(Computer c) {
        super(new SpringLayout());
        ResourceBundle resourceBundle = LocaleResource.getLocaleResourceBundle();

        add(addProperty(resourceBundle.getString("PROPERTIES_DIALOG_IP"), c.getIp()));

        add(addProperty(resourceBundle.getString("PROPERTIES_DIALOG_MAC"), c.getMac()));

        add(addProperty(resourceBundle.getString("PROPERTIES_DIALOG_STATUS"), (c.getStatus() == null ||
                c.getStatus().equals("")) ? resourceBundle.getString("STATUS_NOT_AVAILABLE") : getStatus(c)));

        add(addProperty(resourceBundle.getString("PROPERTIES_DIALOG_ERROR"), (c.getError() == null ||
                c.getError().equals("")) ? resourceBundle.getString("ERROR_NOT_AVAILABLE") : getError(c)));

        SpringUtilities.makeGrid(this, getComponentCount(), 1, 5, 5, 5, 5);
    }
}
