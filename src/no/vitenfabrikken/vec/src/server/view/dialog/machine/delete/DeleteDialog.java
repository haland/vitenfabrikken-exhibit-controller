package no.vitenfabrikken.vec.src.server.view.dialog.machine.delete;

import no.vitenfabrikken.vec.src.library.utils.locale.LocaleResource;
import no.vitenfabrikken.vec.src.library.view.dialog.VECDialog;
import no.vitenfabrikken.vec.src.library.view.layout.SpringUtilities;
import no.vitenfabrikken.vec.src.server.VECServer;
import no.vitenfabrikken.vec.src.server.controller.log.ServerLogger;
import no.vitenfabrikken.vec.src.server.controller.machine.MachineHandler;
import no.vitenfabrikken.vec.src.server.model.list.MachineList;
import no.vitenfabrikken.vec.src.server.model.machine.Machine;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.util.ResourceBundle;
import java.util.logging.Level;


public class DeleteDialog extends VECDialog implements ActionListener, WindowListener {
    private static final long serialVersionUID = -8249326765357232808L;

    private Machine m;
    private JLabel label;
    private JPanel panel;
    private JButton yes, no;
    private JPanel container;
    private MachineList list;

    public DeleteDialog(Machine m, MachineList list) {
        super(VECServer.getInstance().getWindow(),
                LocaleResource.getLocaleResourceBundle().getString("DELETE_DIALOG_TITLE") + " \"" + m.getName() +
                        "\"? - " + LocaleResource.getLocaleResourceBundle().getString("SERVER_APP_TITLE"), true);

        this.m = m;
        this.list = list;

        addWindowListener(this);
        setLayout(new BorderLayout());

        ResourceBundle resourceBundle = LocaleResource.getLocaleResourceBundle();

        label = new JLabel(resourceBundle.getString("DELETE_DIALOG_TEXT") + " \"" + m.getName() + "\"?");

        panel = new JPanel();
        container = new JPanel(new SpringLayout());

        yes = new JButton(resourceBundle.getString("DELETE_DIALOG_YES_BUTTON"));
        yes.addActionListener(this);
        panel.add(yes);

        no = new JButton(resourceBundle.getString("DELETE_DIALOG_NO_BUTTON"));
        no.addActionListener(this);
        panel.add(no);

        container.add(label);
        container.add(panel);
        SpringUtilities.makeGrid(container, container.getComponentCount(), 1, 5, 5, 5, 5);

        add(container, BorderLayout.CENTER);

        pack();
        setPosition();
        setIconImage(VECServer.ICON);

        setVisible(true);
        setResizable(false);
        setAlwaysOnTop(true);
        addWindowListener(this);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(yes)) {
            try {
                list.remove(m);

                File f = new File(MachineHandler.getFilePath(m.getName()));
                f.delete();

                ServerLogger.logMessage(Level.INFO, "Removed machine from the system:'" + m.getName() + "'. Using" +
                        " filepath: '" + f.getAbsolutePath() + "'.");
                System.out.println("Removed machine from the system:'" + m.getName() + "'. Using" +
                        " filepath: '" + f.getAbsolutePath() + "'.");
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
        windowClosing(new WindowEvent(this, 1));
    }

    @Override
    public void windowClosing(WindowEvent e) {
        dispose();
    }

    public void windowOpened(WindowEvent e) {
    }

    public void windowClosed(WindowEvent e) {
    }

    public void windowActivated(WindowEvent e) {
    }

    public void windowIconified(WindowEvent e) {
    }

    public void windowDeactivated(WindowEvent e) {
    }

    public void windowDeiconified(WindowEvent e) {
    }
}
