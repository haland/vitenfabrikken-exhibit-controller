package no.vitenfabrikken.vec.src.server;

import no.vitenfabrikken.vec.src.library.msg.Status;
import no.vitenfabrikken.vec.src.library.utils.inst.InstCtrl;
import no.vitenfabrikken.vec.src.library.utils.inst.listeners.InvokeListener;
import no.vitenfabrikken.vec.src.library.utils.network.channel.Channel;
import no.vitenfabrikken.vec.src.library.utils.network.serializable.Message;
import no.vitenfabrikken.vec.src.server.controller.config.ConfigHandler;
import no.vitenfabrikken.vec.src.server.controller.log.ServerLogger;
import no.vitenfabrikken.vec.src.server.controller.machine.MachineHandler;
import no.vitenfabrikken.vec.src.server.controller.machine.UpdateHandler;
import no.vitenfabrikken.vec.src.server.controller.network.MessageHandler;
import no.vitenfabrikken.vec.src.server.controller.network.NetworkHandler;
import no.vitenfabrikken.vec.src.server.model.config.Configuration;
import no.vitenfabrikken.vec.src.server.view.dialog.settings.SettingsDialog;
import no.vitenfabrikken.vec.src.server.view.window.Window;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.util.logging.Level;

public class VECServer implements InvokeListener, WindowListener {
    public static final String FILE_TYPE = ".vec";
    public static final Image ICON = new ImageIcon("images/tray-icon.png").getImage();
    public static String DATA_FOLDER_PATH = System.getProperty("os.name").toLowerCase().contains("windows") ?
            System.getenv("APPDATA") + File.separator + ".vec-server" :
            System.getProperty("user.home") + File.separator + ".vec-server";
    public static final String MACHINES_FOLDER_PATH = VECServer.DATA_FOLDER_PATH + File.separator + "machines";
    private static VECServer instance;
    private Window window;
    private InstCtrl invoker;
    private Configuration config;
    private UpdateHandler updateHandler;
    private MachineHandler machineHandler;
    private NetworkHandler networkHandler;

    public VECServer() {
        Channel<Status> statusChannel = new Channel<Status>(10);
        Channel<Message> messageChannel = new Channel<Message>(10);

        invoker = new InstCtrl(VECServer.class);
        invoker.addInvokeListener(this);

        config = Configuration.getCurrentConfiguration();

        new ConfigHandler(config);
        new MessageHandler(messageChannel, statusChannel);

        machineHandler = new MachineHandler(statusChannel);
        window = new Window(config, machineHandler.getMachineList());
        window.addWindowListener(this);

        networkHandler = new NetworkHandler(config, invoker, messageChannel);
        updateHandler = new UpdateHandler(networkHandler, machineHandler.getMachineList());
    }

    public static void main(String[] args) {
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                ServerLogger.logStackTrace(Level.SEVERE, e.getStackTrace());
                e.printStackTrace();
            }
        });

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            ServerLogger.logStackTrace(Level.FINE, e.getStackTrace());
            e.printStackTrace();
        }

        File dataFolder = new File(DATA_FOLDER_PATH);
        if (!dataFolder.exists())
            dataFolder.mkdirs();

        File machineFolder = new File(MACHINES_FOLDER_PATH);
        if (!machineFolder.exists())
            machineFolder.mkdirs();

        if (!Configuration.configExists()) {
            new SettingsDialog(null, null);
            getInstance();
        } else {
            getInstance();
        }
    }

    public static VECServer getInstance() {
        if (instance == null)
            instance = new VECServer();
        return instance;
    }

    public Window getWindow() {
        return window;
    }

    public NetworkHandler getNetworkHandler() {
        return networkHandler;
    }

    @Override
    public void instanceInvoked() {
        window.setVisible(true);
        window.setState(Frame.NORMAL);
    }

    public void shutdown() {
        updateHandler.shutdown();
        networkHandler.shutdown();

        System.exit(0);
    }

    @Override
    public void windowClosing(WindowEvent e) {
        shutdown();
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }
}
