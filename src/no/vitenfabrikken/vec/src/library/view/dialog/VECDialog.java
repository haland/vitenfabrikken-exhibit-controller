package no.vitenfabrikken.vec.src.library.view.dialog;

import javax.swing.*;
import javax.swing.plaf.FontUIResource;
import java.awt.*;
import java.awt.geom.Rectangle2D;

public class VECDialog extends JDialog {
    private static final long serialVersionUID = 5226815211454830664L;

    private Window parent;

    public VECDialog() {
        new VECDialog(null);
    }

    public VECDialog(Window window) {
        new VECDialog(window, "");
    }

    public VECDialog(Window window, String title) {
        new VECDialog(window, title, false);
    }

    public VECDialog(Window window, String title, boolean modal) {
        super(window, title);
        setModal(modal);
        parent = window;
    }

    protected void setPosition() {
        int xOrg = Toolkit.getDefaultToolkit().getScreenSize().width / 2;
        int yOrg = Toolkit.getDefaultToolkit().getScreenSize().height / 2;

        if (parent != null) {
            xOrg = parent.getX() + parent.getWidth() / 2;
            yOrg = parent.getY() + parent.getHeight() / 2;
        }

        int xPos = xOrg - getWidth() / 2;
        int yPos = yOrg - getHeight() / 2;

        if (xPos < 0)
            xPos = 0;
        if (yPos < 0)
            yPos = 0;

        setLocation(xPos, yPos);
    }

    @Override
    public void pack() {
        super.pack();

        FontUIResource font = (FontUIResource) UIManager.get("InternalFrame.titleFont");

        if (font != null) {
            int preferredWidth = getWidth();
            int preferredHeight = getHeight();

            FontMetrics fontMatrics = getFontMetrics(font);
            Rectangle2D fontBounds = fontMatrics.getStringBounds(getTitle(), getGraphics());

            if ((fontBounds.getWidth() + 70) > preferredWidth) {
                preferredWidth = (int) fontBounds.getWidth() + 110;

                Dimension dimension = new Dimension(preferredWidth, preferredHeight);

                setPreferredSize(dimension);
                setMinimumSize(dimension);
                super.pack();
            }
        }
    }
}
