package no.vitenfabrikken.vec.src.library.msg;

import java.io.Serializable;

public class Status implements Serializable {
    public static final int ONLINE = 0x01;
    public static final int OFFLINE = 0x02;
    public static final int ALWAYS_ON = 0x03;
    public static final int RESTARTING = 0x04;
    public static final int SHUTTING_DOWN = 0x05;
    public static final int NO_ERROR = 0x50;
    private static final long serialVersionUID = 8329636941284880606L;
    private String ip;
    private String mac;
    private String name;

    private int error;
    private int status;

    public Status(String name, String ip, String mac, int status, int error) {
        this.ip = ip;
        this.mac = mac;
        this.name = name;
        this.error = error;
        this.status = status;
    }

    public String getIp() {
        return ip;
    }

    public String getMac() {
        return mac;
    }

    public String getName() {
        return name;
    }

    public int getError() {
        return error;
    }

    public int getStatus() {
        return status;
    }
}
