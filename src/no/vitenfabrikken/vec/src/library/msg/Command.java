package no.vitenfabrikken.vec.src.library.msg;

import java.io.Serializable;

public class Command implements Serializable {
    public static final String RESTART = "restart";
    public static final String SHUTDOWN = "shutdown";
    public static final String CONFIGURATION = "configuration";
    private static final long serialVersionUID = -2275055321875527590L;
    private String command;

    public Command(String command) {
        this.command = command;
    }

    public String getCommand() {
        return command;
    }
}
