package no.vitenfabrikken.vec.src.library.utils.inst.listeners;

public interface InvokeListener {
    public abstract void instanceInvoked();
}
