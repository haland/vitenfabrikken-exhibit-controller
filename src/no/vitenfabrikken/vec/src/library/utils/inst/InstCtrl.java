package no.vitenfabrikken.vec.src.library.utils.inst;

import no.vitenfabrikken.vec.src.library.utils.inst.listeners.InvokeListener;

import java.util.Iterator;
import java.util.Vector;
import java.util.prefs.Preferences;

public class InstCtrl implements Runnable {
    private static final String THREAD_NAME = "THREAD_NAME";

    private static final int DELAY = 200;
    private static final String KEY = "invoke";

    @SuppressWarnings("rawtypes")
    private Class c;
    private Thread thread;
    private boolean notTerminal;
    private Preferences preferences;
    private Vector<InvokeListener> listeners;

    @SuppressWarnings("rawtypes")
    public InstCtrl(Class c) {
        this.c = c;

        notTerminal = true;
        listeners = new Vector<InvokeListener>();
        preferences = Preferences.userNodeForPackage(c.getClass());

        thread = new Thread(this);
        thread.setName(THREAD_NAME);
        thread.start();
    }

    @SuppressWarnings("rawtypes")
    public static void invokeInstance(Class c, boolean value) {
        Preferences preferences = Preferences.userNodeForPackage(c.getClass());
        preferences.putBoolean(KEY, value);
    }

    @Override
    public void run() {
        while (notTerminal) {
            if (isInstanceInvoked()) {
                System.out.println("invoked");
                invokeInstance(c, false);
                notifyListeners();
            }

            try {
                Thread.sleep(DELAY);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void terminate() {
        notTerminal = false;
    }

    private void notifyListeners() {
        Iterator<InvokeListener> i = listeners.iterator();
        while (i.hasNext())
            i.next().instanceInvoked();
    }

    public void addInvokeListener(InvokeListener l) {
        if (!listeners.contains(l))
            listeners.add(l);
    }

    public void removeInvokeListener(InvokeListener l) {
        if (listeners.contains(l))
            listeners.remove(l);
    }

    public boolean isInstanceInvoked() {
        return preferences.getBoolean(KEY, false);
    }
}
