package no.vitenfabrikken.vec.src.library.utils.locale;

import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;

public class LocaleResource {
    private static final Locale DEFAULT_LOCALE = Locale.US;
    private static final Locale[] SUPPORTED_LOCALES = {DEFAULT_LOCALE, new Locale("nb", "NO")};
    private static ResourceBundle localeResourceBundle;
    private static ResourceBundle[] supportedResourceBundles;

    public static ResourceBundle getLocaleResourceBundle() {
        if (localeResourceBundle == null) {
            Locale locale = null;

            for (Locale i : LocaleResource.SUPPORTED_LOCALES) {
                if (i.getLanguage().equals(Locale.getDefault().getLanguage())) {
                    locale = i;
                    break;
                } else {
                    locale = LocaleResource.DEFAULT_LOCALE;
                }
            }

            localeResourceBundle = ResourceBundle.getBundle("no.vitenfabrikken.vec.resources.Language", locale);
        }

        return localeResourceBundle;
    }

    public static ResourceBundle[] getSupportedResourceBundles() {
        if (supportedResourceBundles == null) {
            ArrayList<ResourceBundle> bundles = new ArrayList<ResourceBundle>();
            for (Locale i : LocaleResource.SUPPORTED_LOCALES) {
                bundles.add(ResourceBundle.getBundle("no.vitenfabrikken.vec.resources.Language", i));
            }

            supportedResourceBundles = bundles.toArray(new ResourceBundle[bundles.size()]);
        }

        return supportedResourceBundles;
    }
}
