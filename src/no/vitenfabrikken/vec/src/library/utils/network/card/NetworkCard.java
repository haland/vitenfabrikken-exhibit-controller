package no.vitenfabrikken.vec.src.library.utils.network.card;

import no.vitenfabrikken.vec.src.library.utils.network.card.exceptions.IllegalMACAddressException;
import no.vitenfabrikken.vec.src.library.utils.network.card.exceptions.NetworkCardNotFoundException;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collections;
import java.util.Enumeration;

public class NetworkCard {
    private MACAddress macAddress;
    private InetAddress networkCard;
    private NetworkInterface networkInterface;

    public NetworkCard(String subNet) throws NetworkCardNotFoundException {
        try {
            Enumeration<NetworkInterface> networks = NetworkInterface.getNetworkInterfaces();

            while (networks.hasMoreElements()) {
                NetworkInterface e = networks.nextElement();

                if (!e.isLoopback() && e.isUp()) {
                    Enumeration<InetAddress> addresses = e.getInetAddresses();

                    for (InetAddress address : Collections.list(addresses)) {
                        if (address.getHostAddress().contains(subNet)) {
                            networkCard = address;
                            networkInterface = e;
                            break;
                        }
                    }
                }

                if (networkCard != null)
                    break;
            }

            if (networkCard == null || networkInterface == null)
                throw new NetworkCardNotFoundException();
            else try {
                macAddress = new MACAddress(networkInterface.getHardwareAddress());
            } catch (NullPointerException e) {
                throw new NetworkCardNotFoundException();
            }

        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IllegalMACAddressException e) {
            e.printStackTrace();
        }
    }

    public static String getSubNetFromIP(String hostIP) {
        String[] split = hostIP.split("\\.");
        return split[0] + "." + split[1] + "." + split[2] + ".";
    }

    public String getIPAddress() {
        return networkCard.getHostAddress();
    }

    public String getMACAddress() {
        return macAddress.toString();
    }
}
