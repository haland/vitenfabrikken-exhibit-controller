package no.vitenfabrikken.vec.src.library.utils.network;

import no.vitenfabrikken.vec.src.library.utils.network.channel.Channel;
import no.vitenfabrikken.vec.src.library.utils.network.serializable.Message;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

class Connection implements Runnable {
    private static final String THREAD_NAME = "Connection";

    private String ip;
    private Socket socket;
    private Thread thread;
    private ObjectInputStream in;
    private ObjectOutputStream out;
    private Channel<Message> channel;

    Connection(String ip, Socket socket, Channel<Message> channel) {
        try {
            this.ip = ip;
            this.socket = socket;
            this.channel = channel;

            out = new ObjectOutputStream(socket.getOutputStream());
            out.flush();

            in = new ObjectInputStream(socket.getInputStream());

            thread = new Thread(this);
            thread.setName(THREAD_NAME);
            thread.start();
        } catch (IOException e) {
            try {
                if (socket != null)
                    socket.close();
            } catch (IOException e1) {
                //e1.printStackTrace();
            }
        }
    }

    void send(Object o) {
        try {
            out.writeObject(o);
            out.flush();
        } catch (IOException e) {
            //e.printStackTrace();
        }
    }

    void close() {
        try {
            if (in != null)
                in.close();
            if (out != null)
                out.close();
            if (socket != null)
                socket.close();
        } catch (IOException e) {
            //e.printStackTrace();
        }
    }

    boolean isClosed() {
        return socket == null || socket.isClosed();
    }

    @Override
    public void run() {
        try {
            while (true) {
                if (socket.isClosed())
                    return;

                Object o = in.readObject();
                channel.put(new Message(ip, o));
            }
        } catch (ClassNotFoundException e) {
            //e.printStackTrace();
        } catch (InterruptedException e) {
            //e.printStackTrace();
        } catch (IOException e) {
            //e.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException e1) {
                //e1.printStackTrace();
            }
        }
    }
}