package no.vitenfabrikken.vec.src.library.utils.network.serializable;

import java.io.Serializable;

public class Message implements Serializable {
    private static final long serialVersionUID = -216945993692809827L;

    private String ip;
    private Object object;

    public Message(String ip, Object object) {
        this.ip = ip;
        this.object = object;
    }

    public String getIP() {
        return ip;
    }

    public Object getObject() {
        return object;
    }
}
