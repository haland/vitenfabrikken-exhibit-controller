package no.vitenfabrikken.vec.src.library.utils.network.card.exceptions;

public class NetworkCardNotFoundException extends Exception {
    private static final long serialVersionUID = 1084434565106230428L;

    public NetworkCardNotFoundException() {
        super();
    }

    public NetworkCardNotFoundException(String e) {
        super(e);
    }
}
