package no.vitenfabrikken.vec.src.library.utils.network;

import java.util.HashMap;
import java.util.Iterator;

class ConnectionMap {
    private HashMap<String, Connection> map;

    ConnectionMap() {
        map = new HashMap<String, Connection>();
    }

    synchronized void add(String key, Connection value) {
        if (map.containsKey(key)) {
            Connection connection = map.get(key);
            if (!connection.isClosed())
                connection.close();
        }

        map.put(key, value);
    }

    synchronized Connection get(String key) {
        if (map.containsKey(key)) {
            Connection c = map.get(key);
            if (!c.isClosed()) {
                return map.get(key);
            } else {
                remove(key);
            }
        }

        return null;
    }

    synchronized void remove(String key) {
        if (map.containsKey(key)) {
            map.get(key).close();
            map.remove(key);
        }
    }

    boolean contains(String key) {
        return map.containsKey(key);
    }

    Iterator<String> iterator() {
        return map.keySet().iterator();
    }
}
