package no.vitenfabrikken.vec.src.library.utils.network.channel;

import java.util.concurrent.ArrayBlockingQueue;

public class Channel<E> extends ArrayBlockingQueue<E> {

    public Channel(int bufferSize) {
        super(bufferSize);
    }
}