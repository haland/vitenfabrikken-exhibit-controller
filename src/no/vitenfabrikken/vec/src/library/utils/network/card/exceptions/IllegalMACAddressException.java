package no.vitenfabrikken.vec.src.library.utils.network.card.exceptions;

public class IllegalMACAddressException extends Exception {
    private static final long serialVersionUID = -829272444072204615L;

    public IllegalMACAddressException() {
        super();
    }

    public IllegalMACAddressException(String e) {
        super(e);
    }
}