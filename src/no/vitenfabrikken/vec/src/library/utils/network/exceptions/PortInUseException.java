package no.vitenfabrikken.vec.src.library.utils.network.exceptions;

public class PortInUseException extends IllegalArgumentException {
    private static final long serialVersionUID = 5002309168112941377L;

    public PortInUseException() {
        super();
    }

    public PortInUseException(String e) {
        super(e);
    }
}
