package no.vitenfabrikken.vec.src.library.utils.network;

import no.vitenfabrikken.vec.src.library.utils.network.channel.Channel;
import no.vitenfabrikken.vec.src.library.utils.network.exceptions.IllegalPortException;
import no.vitenfabrikken.vec.src.library.utils.network.exceptions.PortInUseException;
import no.vitenfabrikken.vec.src.library.utils.network.serializable.Message;

import java.io.IOException;
import java.net.*;
import java.util.Iterator;

public class TCPSocket implements Runnable {
    private static final String THREAD_NAME = "TCPSocket";
    private static final int TIMEOUT = 400;

    private int port;
    private Thread thread;
    private Channel<Message> channel;
    private ServerSocket serverSocket;
    private ConnectionMap connectionMap;

    public TCPSocket(int port, Channel<Message> channel) throws PortInUseException, IllegalPortException, IOException {
        this.port = port;
        this.channel = channel;

        connectionMap = new ConnectionMap();

        try {
            serverSocket = new ServerSocket(port);
            serverSocket.setSoTimeout(TIMEOUT);
        } catch (BindException e) {
            throw new PortInUseException();
        } catch (IllegalArgumentException e) {
            throw new IllegalPortException();
        }

        thread = new Thread(this);
        thread.setName(THREAD_NAME);
        thread.start();
    }

    public static boolean available(int port) throws IllegalArgumentException {
        if (port < 0 || port > 65535)
            throw new IllegalArgumentException("Integer is not a port number. Number is out of range: " + port);

        ServerSocket serverSocket = null;
        DatagramSocket datagramSocket = null;

        try {
            serverSocket = new ServerSocket(port);
            serverSocket.setReuseAddress(true);
            datagramSocket = new DatagramSocket(port);
            datagramSocket.setReuseAddress(true);

            return true;
        } catch (IOException e) {
            //e.printStackTrace();
        } finally {
            if (datagramSocket != null) {
                datagramSocket.close();
            }

            if (serverSocket != null) {
                try {
                    serverSocket.close();
                } catch (IOException e) {
                    //e.printStackTrace();
                }
            }
        }

        return false;
    }

    public synchronized void send(String ip, Object pack) throws ConnectException {
        Connection connection = connectionMap.get(ip);

        if (connection == null) {
            Socket socket = null;

            try {
                socket = new Socket();
                socket.connect(new InetSocketAddress(ip, port), TIMEOUT / 8);
            } catch (IOException e1) {
                try {
                    if (socket != null)
                        socket.close();
                } catch (IOException e2) {
                    //e2.printStackTrace();
                }

                throw new ConnectException();
            }

            connection = new Connection(ip, socket, channel);
            connectionMap.add(ip, connection);
        } else {
            if (connection.isClosed()) {
                connectionMap.remove(ip);
                throw new ConnectException();
            }
        }

        connection.send(pack);
    }

    @Override
    public void run() {
        while (true) {
            try {
                if (serverSocket.isClosed())
                    return;

                try {
                    Socket socket = serverSocket.accept();
                    String ip = socket.getRemoteSocketAddress().toString().replace("/", "").split(":")[0];

                    Connection connection = new Connection(ip, socket, channel);
                    connectionMap.add(ip, connection);
                } catch (SocketTimeoutException e) {
                    //e.printStackTrace();
                }
            } catch (IOException e) {
                //e.printStackTrace();
            }
        }
    }

    public void close() {
        try {
            serverSocket.close();
            Iterator<String> i = connectionMap.iterator();
            while (i.hasNext())
                connectionMap.get(i.next()).close();
        } catch (IOException e) {
            //e.printStackTrace();
        }
    }
}