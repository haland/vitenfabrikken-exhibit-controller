package no.vitenfabrikken.vec.src.library.utils.network.exceptions;

public class IllegalPortException extends IllegalArgumentException {
    private static final long serialVersionUID = -1903126425293428894L;

    public IllegalPortException() {
        super();
    }

    public IllegalPortException(String e) {
        super(e);
    }
}
