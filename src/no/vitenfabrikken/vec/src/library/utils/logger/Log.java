package no.vitenfabrikken.vec.src.library.utils.logger;

import java.io.File;
import java.io.IOException;
import java.util.logging.*;

public class Log {
    private Logger logger;
    private Handler consoleHandler, fileHandler;

    public Log(Level consoleLogLevel, Level fileLogLevel, String fileLogFolderPath, String className) {
        try {
            File folderFile = new File(fileLogFolderPath);
            if (!folderFile.isDirectory()) {
                folderFile.mkdir();
            }

            consoleHandler = new ConsoleHandler();
            consoleHandler.setLevel(consoleLogLevel);
            consoleHandler.setFormatter(new SimpleFormatter());

            fileHandler = new FileHandler(fileLogFolderPath + File.separator + "error.log", true);
            fileHandler.setLevel(fileLogLevel);
            fileHandler.setFormatter(new SimpleFormatter());

            logger = Logger.getLogger(className);
            logger.setUseParentHandlers(false);
            logger.setLevel(Level.ALL);
            logger.addHandler(fileHandler);
            logger.addHandler(consoleHandler);
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeMessage(Level level, String message) {
        logger.log(level, message);
    }

    public void writeStackTrace(Level level, StackTraceElement[] stackTraceElements) {
        String stackTrace = "";
        for (StackTraceElement stackTraceElement : stackTraceElements) {
            stackTrace = stackTrace + stackTraceElement.getClassName() + "\n" +
                    "   at " + stackTraceElement.getClassName() + "." +
                    stackTraceElement.getMethodName() + "(" + stackTraceElement.getFileName() +
                    ":" + stackTraceElement.getLineNumber() + ")\n";
        }

        logger.log(level, stackTrace);
    }
}
