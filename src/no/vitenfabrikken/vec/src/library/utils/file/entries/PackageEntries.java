package no.vitenfabrikken.vec.src.library.utils.file.entries;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class PackageEntries {
    /**
     * Lists all classes located in a package. Works for current program and .jar. Added functionality for reading
     * classes while launching from Eclipse.
     *
     * @param packageName - The package to search
     * @return List of classes in the given package
     * @author Will Glass (original author)
     * @author Chris H�land (minor changes)
     */
    public static ArrayList<String> getClassNamesFromPackage(String packageName) {
        try {
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            URL packageURL;
            ArrayList<String> names = new ArrayList<String>();
            ;

            packageName = packageName.replace(".", "/");
            packageURL = classLoader.getResource(packageName);

            if (packageURL.getProtocol().equals("jar")) {
                String jarFileName;
                JarFile jf;
                Enumeration<JarEntry> jarEntries;
                String entryName;

                jarFileName = URLDecoder.decode(packageURL.getFile(), "UTF-8");
                jarFileName = jarFileName.substring(5, jarFileName.indexOf("!"));

                jf = new JarFile(jarFileName);
                jarEntries = jf.entries();

                while (jarEntries.hasMoreElements()) {
                    JarEntry entry = jarEntries.nextElement();
                    if (!entry.isDirectory()) {
                        entryName = entry.getName();
                        if (entryName.startsWith(packageName) && entryName.length() > packageName.length() + 5) {
                            if (entryName.contains(".class") || entryName.contains(".java")) {
                                entryName = entryName.substring(packageName.length(), entryName.lastIndexOf('.'));
                                if (!entryName.contains("/"))
                                    names.add(entryName);
                            }
                        }
                    }
                }

                jf.close();

            } else {
                File folder = new File(packageURL.getFile());
                File[] contenuti;
                if (folder.listFiles() == null)
                    folder = new File("bin/" + packageName);

                contenuti = folder.listFiles();

                String entryName;
                for (File actual : contenuti) {
                    entryName = actual.getName();
                    if (entryName.contains(".class") || entryName.contains(".java")) {
                        entryName = entryName.substring(0, entryName.lastIndexOf('.'));
                        names.add(entryName);
                    }
                }
            }
            return names;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
