package no.vitenfabrikken.vec.resources;

import java.util.ListResourceBundle;

public class Language extends ListResourceBundle {
    @Override
    protected Object[][] getContents() {
        return new Object[][]{
                //Locale name
                {"LOCALE_NAME", "English"},

                //Client application
                {"CLIENT_APP_TITLE", "VEC Client"},

                {"CLIENT_SETTINGS_TITLE", "Settings - VEC Client"},
                {"CLIENT_SETTINGS_DESCRIPTION_NAME", "The name of the computer/exhibit post"},
                {"CLIENT_SETTINGS_HOST_IP", "IP address of the server"},
                {"CLIENT_SETTINGS_PORT", "Communication port"},
                {"CLIENT_SETTINGS_ALWAYS_ONLINE", "Keep computer online, disabling shutdown and restart command"},
                {"CLIENT_SETTINGS_SAVE", "Save"},
                {"CLIENT_SETTINGS_CANCEL", "Cancel"},
                {"CLIENT_SETTINGS_ILLEGAL_PORT", "Please enter a valid port"},

                {"CLIENT_TRAY_EXIT", "Exit"},
                {"CLIENT_TRAY_CONNECT", "Connect"},
                {"CLIENT_TRAY_DISCONNECT", "Disconnect"},
                {"CLIENT_TRAY_SETTINGS", "Settings"},

                {"INSTALLATION_DIALOG_TITLE", "TightVNC Installation - VEC Client"},
                {"INSTALLATION_DIALOG_TEXT", "Would you like to install TightVNC for remote accessing this computer?"},
                {"INSTALLATION_DIALOG_YES_BUTTON", "Yes"},
                {"INSTALLATION_DIALOG_NO_BUTTON", "No"},

                {"NETWORK_CARD_NOT_FOUND_PART_1", "No network card found that can reach the server address (IP: "},
                {"NETWORK_CARD_NOT_FOUND_PART_2", "). Please enter a correct address."},

                //Server application
                {"SERVER_APP_TITLE", "VEC Server"},

                {"SERVER_SETTINGS_TITLE", "Settings - VEC Server"},
                {"SERVER_SETTINGS_PORT", "Port"},
                {"SERVER_SETTINGS_SAVE", "Save"},
                {"SERVER_SETTINGS_CANCEL", "Cancel"},
                {"SERVER_SETTINGS_ALWAYS_ONLINE", "Status text for machines that are always on"},
                {"SERVER_SETTINGS_ILLEGAL_PORT", "Please enter a valid port"},

                {"SERVER_TRAY_OPEN", "Open"},
                {"SERVER_TRAY_EXIT", "Exit"},
                {"SERVER_TRAY_CONNECT", "Connect"},
                {"SERVER_TRAY_DISCONNECT", "Disconnect"},
                {"SERVER_TRAY_SETTINGS", "Settings"},

                {"EXIT_DIALOG_TITLE", "Exit program? - VEC Server"},
                {"EXIT_DIALOG_TEXT", "Are you sure you want to quit the program?"},
                {"EXIT_DIALOG_YES_BUTTON", "Yes"},
                {"EXIT_DIALOG_NO_BUTTON", "No"},

                {"PROPERTIES_DIALOG_TITLE", "Properties for"},
                {"PROPERTIES_DIALOG_IP", "IP address:"},
                {"PROPERTIES_DIALOG_PORT", "Port:"},
                {"PROPERTIES_DIALOG_MAC", "MAC address:"},
                {"PROPERTIES_DIALOG_TYPE", "Type:"},
                {"PROPERTIES_DIALOG_STATUS", "Status:"},
                {"PROPERTIES_DIALOG_ERROR", "Error:"},
                {"PROPERTIES_DIALOG_USERNAME", "Username:"},
                {"PROPERTIES_DIALOG_PASSWORD", "Password:"},
                {"PROPERTIES_DIALOG_LAMP_HOURS_TITLE", "Lamp hours:"},
                {"PROPERTIES_DIALOG_REMAINING_LAMP_HOURS_SUBTEXT", ", remaining hours: "},
                {"PROPERTIES_DIALOG_FILTER_HOURS_TITLE", "Filter hours:"},
                {"PROPERTIES_DIALOG_OK_BUTTON", "OK"},

                {"DELETE_DIALOG_TITLE", "Remove"},
                {"DELETE_DIALOG_TEXT", "Are you sure you want to remove"},
                {"DELETE_DIALOG_YES_BUTTON", "Yes"},
                {"DELETE_DIALOG_NO_BUTTON", "No"},

                {"ADD_PROJECTOR_TITLE", "Add projector - VEC Server"},
                {"ADD_PROJECTOR_SAVE_BUTTON", "Save"},
                {"ADD_PROJECTOR_CANCEL_BUTTON", "Cancel"},
                {"ADD_PROJECTOR_NAME_TEXT", "The name of the projector/exhibit post"},
                {"ADD_PROJECTOR_IP_TEXT", "IP address (255.255.255.255)"},
                {"ADD_PROJECTOR_PORT_TEXT", "Port"},
                {"ADD_PROJECTOR_MAC_TEXT", "MAC address (00:00:00:00:00:00)"},
                {"ADD_PROJECTOR_TYPE_TEXT", "Choose the projector type"},
                {"ADD_PROJECTOR_USERNAME_TEXT", "Username for the projectors web-interface (leave blank if no login is required)"},
                {"ADD_PROJECTOR_PASSWORD_TEXT", "Password for the projectors web-interface (leave blank if no login is required)"},

                {"MACHINE_WAKE_LISTENER_TITLE", "Starting up..."},
                {"MACHINE_SHUTDOWN_LISTENER_TITLE", "Shutting down..."},
                {"MACHINE_RESTART_LISTENER_TITLE", "Restarting..."},

                {"STATUS_ERROR", "error"},
                {"STATUS_ONLINE", "online"},
                {"STATUS_OFFLINE", "offline"},
                {"STATUS_ALWAYS_ON", "always on"},
                {"STATUS_RESTARTING", "restarting"},
                {"STATUS_STARTING_UP", "starting up"},
                {"STATUS_SHUTTING_DOWN", "shutting down"},
                {"STATUS_NOT_AVAILABLE", "N/A"},

                {"ERROR_NO_ERROR", "no error"},
                {"ERROR_NOT_AVAILABLE", "N/A"},
                {"ERROR_UNABLE_TO_START", "timeout on startup"},

                {"MACHINE_STATUS_PART_1", " ("},
                {"MACHINE_STATUS_PART_2", ", "},
                {"MACHINE_STATUS_PART_3", ")"},

                {"MACHINE_POPUP_MENU_TURN_ON", "Turn on"},
                {"MACHINE_POPUP_MENU_TURN_OFF", "Turn off"},
                {"MACHINE_POPUP_MENU_RESTART", "Restart"},
                {"MACHINE_POPUP_MENU_REMOVE", "Remove"},
                {"MACHINE_POPUP_MENU_REMOTE", "Remote, using TightVNC"},
                {"MACHINE_POPUP_MENU_PROPERTIES", "Properties"},

                {"UNIVERSAL_TURN_ALL_ON", "Turn all on"},
                {"UNIVERSAL_TURN_ALL_OFF", "Turn all off"},

                {"OPTIONS_MENU", "Options"},
                {"OPTIONS_MENU_SETTINGS", "Settings"},

                {"FILE_MENU", "File"},
                {"FILE_MENU_EXIT", "Exit"},

                {"EDIT_MENU", "Edit"},
                {"EDIT_MENU_ADD_PROJECTOR", "Add projector..."},

                //Projectors
                {"PROJECTOR_NO_CONNECTION", "can not be reached over network"},
                {"PROJECTOR_ON", "on"},
                {"PROJECTOR_OFF", "off"},
                {"PROJECTOR_COOLING_DOWN", "cooling down"},
                {"PROJECTOR_POWERING_ON", "powering on"},
                {"PROJECTOR_POWERING_OFF", "powering off"},
                {"PROJECTOR_CANT_POWER_OFF", "can�t power off yet"},
                {"PROJECTOR_NO_ERROR", "no errors"},
                {"PROJECTOR_LAMP_COVER_ERROR", "lamp cover error"},
                {"PROJECTOR_TEMPERATURE_ERROR", "temperature error"},
                {"PROJECTOR_FAN_ERROR", "fan error"},
                {"PROJECTOR_POWER_ERROR", "power error"},
                {"PROJECTOR_LAMP_ERROR", "lamp error"},
                {"PROJECTOR_LAMP_EOL", "lamp has reached its end of life"},
                {"PROJECTOR_ERROR_CODE", "error code "},
                {"PROJECTOR_NO_LAMP", "lamp is missing"},
        };
    }
}
