package no.vitenfabrikken.vec.resources;

import java.util.ListResourceBundle;

public class Language_nb extends ListResourceBundle {

    @Override
    protected Object[][] getContents() {
        return new Object[][]{
                //Locale name
                {"LOCALE_NAME", "Norsk"},

                //Client application
                {"CLIENT_APP_TITLE", "VEC Client"},

                {"CLIENT_SETTINGS_TITLE", "Innstillinger - VEC Client"},
                {"CLIENT_SETTINGS_DESCRIPTION_NAME", "Navngi maskinen"},
                {"CLIENT_SETTINGS_HOST_IP", "IP-adressen til serveren"},
                {"CLIENT_SETTINGS_PORT", "Port"},
                {"CLIENT_SETTINGS_ALWAYS_ONLINE", "La datamaskinen alltid være på og deaktiver \"slå av\" - og \"restart-kommandoen."},
                {"CLIENT_SETTINGS_SAVE", "Lagre"},
                {"CLIENT_SETTINGS_CANCEL", "Avbryt"},
                {"CLIENT_SETTINGS_ILLEGAL_PORT", "Please enter a valid port"},

                {"CLIENT_TRAY_EXIT", "Avslutt"},
                {"CLIENT_TRAY_CONNECT", "Koble til"},
                {"CLIENT_TRAY_DISCONNECT", "Koble fra"},
                {"CLIENT_TRAY_SETTINGS", "Innstillinger"},

                {"INSTALLATION_DIALOG_TITLE", "TightVNC installasjon - VEC Client"},
                {"INSTALLATION_DIALOG_TEXT", "Vil du installere TightVNC for kunne fjernstyre denne maskinen?"},
                {"INSTALLATION_DIALOG_YES_BUTTON", "Ja"},
                {"INSTALLATION_DIALOG_NO_BUTTON", "Nei"},

                {"NETWORK_CARD_NOT_FOUND_PART_1", "Ingen nettverkskort funnet på denne maskinen som kan nå serveren (IP: "},
                {"NETWORK_CARD_NOT_FOUND_PART_2", "). Vennligst skriv inn en korrekt IP-adresse eller endre nettverksinnstillinger for maskinen."},

                //Server application
                {"SERVER_APP_TITLE", "VEC Server"},

                {"SERVER_SETTINGS_TITLE", "Innstillinger - VEC Server"},
                {"SERVER_SETTINGS_PORT", "Port"},
                {"SERVER_SETTINGS_SAVE", "Lagre"},
                {"SERVER_SETTINGS_CANCEL", "Avbryt"},
                {"SERVER_SETTINGS_ALWAYS_ONLINE", "Statustekst for maskiner som alltid er på"},
                {"SERVER_SETTINGS_ILLEGAL_PORT", "Oppgi et korrekt portnummer"},

                {"SERVER_TRAY_OPEN", "Åpne"},
                {"SERVER_TRAY_EXIT", "Avslutt"},
                {"SERVER_TRAY_CONNECT", "Koble til"},
                {"SERVER_TRAY_DISCONNECT", "Koble fra"},
                {"SERVER_TRAY_SETTINGS", "Innstillinger"},

                {"EXIT_DIALOG_TITLE", "Avslutte programmet? - VEC Server"},
                {"EXIT_DIALOG_TEXT", "Er du sikker på at du vil avslutte programmet?"},
                {"EXIT_DIALOG_YES_BUTTON", "Ja"},
                {"EXIT_DIALOG_NO_BUTTON", "Nei"},

                {"PROPERTIES_DIALOG_TITLE", "Status for"},
                {"PROPERTIES_DIALOG_IP", "IP-adresse:"},
                {"PROPERTIES_DIALOG_PORT", "Port:"},
                {"PROPERTIES_DIALOG_MAC", "MAC-adresse:"},
                {"PROPERTIES_DIALOG_TYPE", "Type:"},
                {"PROPERTIES_DIALOG_STATUS", "Status:"},
                {"PROPERTIES_DIALOG_ERROR", "Feil:"},
                {"PROPERTIES_DIALOG_USERNAME", "Brukernavn:"},
                {"PROPERTIES_DIALOG_PASSWORD", "Passord:"},
                {"PROPERTIES_DIALOG_LAMP_HOURS_TITLE", "Lampetimer:"},
                {"PROPERTIES_DIALOG_REMAINING_LAMP_HOURS_SUBTEXT", ", gjenværende timer: "},
                {"PROPERTIES_DIALOG_FILTER_HOURS_TITLE", "Filtertimer:"},
                {"PROPERTIES_DIALOG_OK_BUTTON", "OK"},

                {"DELETE_DIALOG_TITLE", "Slett"},
                {"DELETE_DIALOG_TEXT", "Er du sikker på at du vil slette"},
                {"DELETE_DIALOG_YES_BUTTON", "Ja"},
                {"DELETE_DIALOG_NO_BUTTON", "Nei"},

                {"ADD_PROJECTOR_TITLE", "Legg til projektor - VEC Server"},
                {"ADD_PROJECTOR_SAVE_BUTTON", "Lagre"},
                {"ADD_PROJECTOR_CANCEL_BUTTON", "Avbryt"},
                {"ADD_PROJECTOR_NAME_TEXT", "Navngi projektoren"},
                {"ADD_PROJECTOR_IP_TEXT", "IP-adresse (255.255.255.255)"},
                {"ADD_PROJECTOR_PORT_TEXT", "Port"},
                {"ADD_PROJECTOR_MAC_TEXT", "MAC-adresse (00:00:00:00:00:00)"},
                {"ADD_PROJECTOR_TYPE_TEXT", "Velg projektortype"},
                {"ADD_PROJECTOR_USERNAME_TEXT", "Brukernavn for projektorens web-grensesnitt"},
                {"ADD_PROJECTOR_PASSWORD_TEXT", "Passord for projektorens web-grensesnitt"},

                {"MACHINE_WAKE_LISTENER_TITLE", "Starter..."},
                {"MACHINE_SHUTDOWN_LISTENER_TITLE", "Slår av..."},
                {"MACHINE_RESTART_LISTENER_TITLE", "Restarter..."},

                {"STATUS_ERROR", "feil"},
                {"STATUS_ONLINE", "på"},
                {"STATUS_OFFLINE", "av"},
                {"STATUS_ALWAYS_ON", "alltid på"},
                {"STATUS_RESTARTING", "restarer"},
                {"STATUS_STARTING_UP", "starter"},
                {"STATUS_SHUTTING_DOWN", "slås av"},
                {"STATUS_NOT_AVAILABLE", "N/A"},

                {"ERROR_NO_ERROR", "ingen feil"},
                {"ERROR_NOT_AVAILABLE", "N/A"},
                {"ERROR_UNABLE_TO_START", "ikke startet skikkelig"},

                {"MACHINE_STATUS_PART_1", " ("},
                {"MACHINE_STATUS_PART_2", ", "},
                {"MACHINE_STATUS_PART_3", ")"},

                {"MACHINE_POPUP_MENU_TURN_ON", "Start"},
                {"MACHINE_POPUP_MENU_TURN_OFF", "Slå av"},
                {"MACHINE_POPUP_MENU_RESTART", "Restart"},
                {"MACHINE_POPUP_MENU_REMOVE", "Slett"},
                {"MACHINE_POPUP_MENU_REMOTE", "Fjerstyr, med bruk av TightVNC"},
                {"MACHINE_POPUP_MENU_PROPERTIES", "Status"},

                {"UNIVERSAL_TURN_ALL_ON", "Start alle maskiner"},
                {"UNIVERSAL_TURN_ALL_OFF", "Slå av alle maskiner"},

                {"OPTIONS_MENU", "Valg"},
                {"OPTIONS_MENU_SETTINGS", "Innstillinger"},

                {"FILE_MENU", "Fil"},
                {"FILE_MENU_EXIT", "Avslutt"},

                {"EDIT_MENU", "Rediger"},
                {"EDIT_MENU_ADD_PROJECTOR", "Legg til projektor..."},

                //Projectors
                {"PROJECTOR_NO_CONNECTION", "kan ikke nås over nettverket"},
                {"PROJECTOR_ON", "på"},
                {"PROJECTOR_OFF", "av"},
                {"PROJECTOR_COOLING_DOWN", "kjøler ned"},
                {"PROJECTOR_POWERING_ON", "slås på"},
                {"PROJECTOR_POWERING_OFF", "slås av"},
                {"PROJECTOR_CANT_POWER_OFF", "kan ikke slås av enda"},
                {"PROJECTOR_NO_ERROR", "ingen feil"},
                {"PROJECTOR_LAMP_COVER_ERROR", "feil med lampedekselet"},
                {"PROJECTOR_TEMPERATURE_ERROR", "temperaturfeil"},
                {"PROJECTOR_FAN_ERROR", "viftefeil"},
                {"PROJECTOR_POWER_ERROR", "strømfeil"},
                {"PROJECTOR_LAMP_ERROR", "lampefeil"},
                {"PROJECTOR_LAMP_EOL", "lampen har nådd enden av livsløpet sitt"},
                {"PROJECTOR_ERROR_CODE", "feilkode "},
                {"PROJECTOR_NO_LAMP", "mangler projektorpære"},
        };
    }
}
